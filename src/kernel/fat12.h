#pragma once
#include "vfs.h"
#include "fs_constants.h"
#include "fat.h"
#include "files.h"
#include "file_in_memory.h"

class Fat_FS : public VFS {

public:
    
    Fat_FS(const Fat_FS&) = delete;

    explicit Fat_FS(uint8_t disk_number, kiv_hal::TDrive_Parameters parameters);

    bool Init();

    kiv_os::NOS_Error Fat_FS::Create(uint8_t disk_number, uint8_t attributes, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table);
    kiv_os::NOS_Error Read_Dir(uint8_t disk_number, uint8_t attributes, Directory_Entry& item, std::vector<uint16_t> fat_table, std::vector<kiv_os::TDir_Entry>& entries);

  //override methods
    kiv_os::NOS_Error Remove(fs::path path) override;                           // Removes directory / rmdir

    kiv_os::NOS_Error Open(fs::path path, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, std::shared_ptr<File>& file) override;

    kiv_os::NOS_Error Set_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) override;

    kiv_os::NOS_Error Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) override;

    kiv_os::NOS_Error Close(fs::path path) override;


    std::mutex fat_fs_mutex;

    uint8_t disk_number;

    kiv_hal::TDrive_Parameters disk_parameters;

    std::vector<uint16_t> converted_fat;

   // std::recursive_mutex mutex_fs;

};

class Fat_File : public File {

public:

    Fat_File(uint8_t disk_num, kiv_hal::TDrive_Parameters params, fs::path path, std::vector<uint16_t>& fs)
        : disk_number(disk_num), disk_parameters(params), path(path), converted_fat(fs) {}


    kiv_os::NOS_Error Write(char* buffer, size_t size, size_t& written) override;

    kiv_os::NOS_Error Read(char* buffer, size_t size, size_t& read) override;

    std::recursive_mutex mutex;

    uint8_t disk_number;

    kiv_hal::TDrive_Parameters disk_parameters;
   
    fs::path path;

    std::vector<uint16_t> converted_fat;

    size_t position{0};

};
