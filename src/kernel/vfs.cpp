#include "proc_fs.h"
#include "fat12.h"
#include <iostream>
#include <fstream>


//initialization filesystem 

void File_System::Init() {
   
   // file_systems['P'] = std::make_unique<Proc_FS>();
    file_systems["C:\\procfs"] = std::unique_ptr<VFS>(new Proc_FS());

    kiv_hal::TRegisters regs{};
    for (int i = 0; i < 256; i++) {

        kiv_hal::TDrive_Parameters params{};
        regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Drive_Parameters);
        regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(&params);

        kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);                   //interrupt, call disk for obtaining parameters
        if (!regs.flags.carry) {
            uint8_t disk_num = regs.rdx.l;                                                     // 
            auto disk_params = reinterpret_cast<kiv_hal::TDrive_Parameters*>(regs.rdi.r);
            auto fat = new Fat_FS(disk_num, *disk_params);
            //fat->Fat_FS::Init();
            file_systems["C"] = std::unique_ptr<VFS>(fat);
           // break;
        }
        regs.rdx.l++;
    }
    
}


VFS* File_System::Get_File_System(fs::path path) {

    if(!path.has_root_name()) return nullptr;
    std::string disk;
    if (path.string().compare(0, 10, "C:\\procfs") == 0)
    { 
        disk = "C:\\procfs";
    }
    else {   
        disk = path.root_name().string().at(0);
        //std::string s(1, disk.at(0));
    }

    //auto resolved = file_systems.count(s);

    //if (resolved != 0) {
        return file_systems[disk].get();
  //  }
   // return nullptr;
}

kiv_os::NOS_Error File_System::Open(fs::path path, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, std::shared_ptr<File>& file) {
    VFS* fs = Get_File_System(path);
    return fs ? fs->Open(path, flags, attributes, file) : kiv_os::NOS_Error::Unknown_Filesystem;
}

kiv_os::NOS_Error File_System::Set_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) {
    VFS* fs = Get_File_System(path);
    return fs ? fs->Set_File_Attributes(path, attributes) : kiv_os::NOS_Error::Unknown_Filesystem;
}

kiv_os::NOS_Error File_System::Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) {
    VFS* fs = Get_File_System(path);
    return fs ? fs->Get_File_Attributes(path, attributes) : kiv_os::NOS_Error::Unknown_Filesystem;
}

kiv_os::NOS_Error File_System::Remove(fs::path path) {
    VFS* fs = Get_File_System(path);
    return fs ? fs->Remove(path) : kiv_os::NOS_Error::Unknown_Filesystem;
}

kiv_os::NOS_Error File_System::Close(fs::path path) {
    VFS* fs = Get_File_System(path);
    return fs ? fs->Close(path) : kiv_os::NOS_Error::Unknown_Filesystem;
}