#include <vector>
#include "process.h"
#include <memory>
#include <iostream>


#include <vector>
#include "process.h"
#include <memory>
#include <iostream>


const auto DEFAULT_WORKING_DIRECTORY = "C:\\";


std::shared_ptr<Process>
Process_Control_Block::Add_Process(kiv_os::THandle handle, kiv_os::THandle std_in, kiv_os::THandle std_out,
    std::string program_name) {
    std::lock_guard<std::recursive_mutex> guard(pcb_mutex);
    table[handle] = std::make_shared<Process>(handle, std_in, std_out, program_name, DEFAULT_WORKING_DIRECTORY);
    return table[handle];
}

void Process_Control_Block::Remove_Process(kiv_os::THandle handle) {
    std::lock_guard<std::recursive_mutex> guard(pcb_mutex);
    table.erase(handle);
}

std::shared_ptr<Process> Process_Control_Block::Find_Process(kiv_os::THandle handle) {
    std::lock_guard<std::recursive_mutex> guard(pcb_mutex);
    auto entry = table.find(handle);
    return entry != table.end() ? entry->second : nullptr;
}

std::vector<std::shared_ptr<Process>> Process_Control_Block::Get_Processes() {
    std::lock_guard<std::recursive_mutex> guard(pcb_mutex);
    std::vector<std::shared_ptr<Process>> processes;

    for (const auto& entry : table) {
        processes.push_back(entry.second);
    }

    return processes;
}
