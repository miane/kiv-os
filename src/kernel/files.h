#pragma once
#include <map>
#include <memory>
#include <mutex>
#include "handles.h"
#include "../api/api.h"
#include <vector>


class File {

public:
    virtual kiv_os::NOS_Error Write(char* buffer, size_t size, size_t& written) = 0;

    virtual kiv_os::NOS_Error Read(char* buffer, size_t size, size_t& written) = 0;

    virtual void Close() {};
    
    virtual ~File() {};
   
};

class File_Handles : public File {
public:

    File_Handles() = default;
    kiv_os::NOS_Error Write(char* buffer, size_t size, size_t& written);
    kiv_os::NOS_Error Read(char* buffer, size_t size, size_t& read);

    kiv_os::THandle Add_File(std::shared_ptr<File> file);
    std::shared_ptr<File> Get_File(kiv_os::THandle handle);
    bool Delete_File(kiv_os::THandle);

    std::map<kiv_os::THandle, std::shared_ptr<File>> table; //
    std::mutex table_mutex;
    std::recursive_mutex rec_mutex;//

};
/*
kiv_os::THandle Add_File_To_FileHandles(std::shared_ptr<File> file);
std::shared_ptr<File> Get_File_From_FileHandles(kiv_os::THandle handle);
*/