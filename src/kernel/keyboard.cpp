#include <iostream>
#include <memory>
#include "keyboard.h"

kiv_os::NOS_Error Keyboard::Write(char* buffer, size_t size, size_t& written) {
    kiv_hal::TRegisters regs{};
    regs.rax.h = static_cast<decltype(regs.rax.h)>(kiv_hal::NVGA_BIOS::Write_String);
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(buffer);
    regs.rcx.r = static_cast<decltype(regs.rcx.r)>(size);

    /*
    for (i = 0; i < size; i++) {
        regs.rax.l = buffer[i];
        regs.rax.h = static_cast<decltype(regs.rax.h)>(kiv_hal::NKeyboard::Write_Char);
        kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Keyboard, regs);
        if (!regs.flags.non_zero) {
            error = kiv_os::NOS_Error::IO_Error;
            break;
        }
    }
    */

    if (regs.rax.r == 0) {
        written = 0;
        return kiv_os::NOS_Error::IO_Error;
    }
    else {
        written = size;
        return kiv_os::NOS_Error::Success;
    }
}

kiv_os::NOS_Error Keyboard::Read(char* buffer, size_t size, size_t& read) {
    std::lock_guard<decltype(mutex)> lck(mutex);
    
    kiv_hal::TRegisters regs{};
      
    char* out_buffer = reinterpret_cast<char*>(buffer);

    size_t pos = 0;
    while (pos < size) {

        regs.rax.h = static_cast<decltype(regs.rax.l)>(kiv_hal::NKeyboard::Read_Char);
        kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Keyboard, regs);

        if (!regs.flags.non_zero) {
            break; // nothing has been read
        }

        char ch = regs.rax.l;
        switch (static_cast<kiv_hal::NControl_Codes>(ch)) {

        case kiv_hal::NControl_Codes::BS: {
            if (pos > 0) {
                pos--;
            }

            regs.rax.h = static_cast<decltype(regs.rax.l)>(kiv_hal::NVGA_BIOS::Write_Control_Char);
            regs.rdx.l = ch;
            kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, regs);
            break;
        }

        case kiv_hal::NControl_Codes::LF: break;
        case kiv_hal::NControl_Codes::NUL:
        case kiv_hal::NControl_Codes::EOT:
        case kiv_hal::NControl_Codes::CR: {
            out_buffer[pos] = ch;
            pos++;
            read = pos;
            return kiv_os::NOS_Error::Success;
        }
        default: {
            out_buffer[pos] = ch;
            pos++;
            regs.rax.h = static_cast<decltype(regs.rax.l)>(kiv_hal::NVGA_BIOS::Write_String);
            regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(&ch);
            regs.rcx.r = 1;
            kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, regs);
            break;
        }
        }
        read = pos;
    }

    return kiv_os::NOS_Error::Success;
}