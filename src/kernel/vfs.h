#pragma once

#include <vector>
#include "../api/api.h"
#include <string>
#include <filesystem>
#include <map>
#include "files.h"


namespace fs = std::filesystem;


class VFS {
public:

   
    //Removes directory / rmdir
    virtual kiv_os::NOS_Error Remove(fs::path path) = 0;

    virtual kiv_os::NOS_Error Open(fs::path path, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, std::shared_ptr<File>& file) = 0;

    virtual kiv_os::NOS_Error Set_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) = 0;

    virtual kiv_os::NOS_Error Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) = 0;

    virtual kiv_os::NOS_Error Close(fs::path path) = 0;
    
};

class File_System : public VFS {

public:
    File_System() = default;

    void Init();
   
    //Removes directory / rmdir
    kiv_os::NOS_Error Remove(fs::path path) override;

    kiv_os::NOS_Error Open(fs::path path, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, std::shared_ptr<File>& file) override;

    kiv_os::NOS_Error Set_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) override;

    kiv_os::NOS_Error Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) override;

    kiv_os::NOS_Error Close(fs::path path) override;

private:
    std::map<std::string, std::unique_ptr<VFS>> file_systems;

   // std::map<char, std::unique_ptr<VFS>> file_systems;

    VFS* Get_File_System(fs::path path);

};
