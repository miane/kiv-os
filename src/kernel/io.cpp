#include "io.h"
#include "kernel.h"
#include "handles.h"
#include "files.h"
#include "pipe.h"
#include "keyboard.h"
#include "console.h"
#include <fstream>
#include "process.h"
#include "proc_fs.h"
#include <filesystem>


std::map<std::string, std::unique_ptr<VFS>> File_Systems;
File_Handles file_handles;
kiv_os::THandle Console_In = kiv_os::Invalid_Handle;
kiv_os::THandle Console_Out = kiv_os::Invalid_Handle;


void Initialize_IO() {
	std::lock_guard<std::mutex> lck_st(file_handles.table_mutex);

	kiv_os::THandle cin_hnd = 1;
	file_handles.table.emplace(cin_hnd, std::make_unique<Keyboard>());

	kiv_os::THandle cout_hnd = 2;
	file_handles.table.emplace(cout_hnd, std::make_unique<Console>());


	Console_In = cin_hnd;
	Console_Out = cout_hnd;

}

kiv_os::THandle Get_Console_In() {
	return Console_In;
}

kiv_os::THandle Get_Console_Out() {
	return Console_Out;
}

void Open_File(kiv_hal::TRegisters& regs) {
	std::lock_guard<std::mutex> lck_st(file_handles.table_mutex);
	auto file_name = reinterpret_cast<const char*>(regs.rdx.r);
	kiv_os::NOpen_File flags = static_cast<kiv_os::NOpen_File>(regs.rcx.l);
	kiv_os::NFile_Attributes attributes = static_cast<kiv_os::NFile_Attributes>(regs.rdi.i);

	auto proc_handle = Get_Current_Process_Handle();
	std::filesystem::path filename(file_name);

	//if (!path.is_absolute() && !path.has_root_name()) {
	fs::path path = Process_working_Dir();
	path += filename;
	//}

	path = (path.root_path() / path.relative_path()).lexically_normal();
	std::shared_ptr<File> file;
	auto error = Get_File_System()->Open(path, flags, attributes, file);
	
	if (error != kiv_os::NOS_Error::Success) {
		regs.rax.x = static_cast<uint16_t>(kiv_os::Invalid_Handle);
		return;
	}
	kiv_os::THandle file_handle = file_handles.Add_File(file);
	regs.rax.x = static_cast<uint16_t>(file_handle);

	Add_Owned_Handle(proc_handle, file_handle);
}

kiv_os::THandle Open_Shell(const char* input_file_name, kiv_os::NOpen_File flags, uint8_t attributes, kiv_os::NOS_Error & error) {
		std::lock_guard<std::mutex> guard(file_handles.table_mutex);

		//File* file = nullptr;

		if (strcmp(input_file_name, "\\sys\\tty") == 0) {
			std::shared_ptr<Console> file = std::make_shared<Console>();
			//file = new Console();
			return file_handles.Add_File(file);
		}
		else if (strcmp(input_file_name, "\\sys\\kb") == 0) {
			std::shared_ptr<Keyboard> file = std::make_shared<Keyboard>();
			//file = new Keyboard();
			return file_handles.Add_File(file);
		}

		else
		{
			return kiv_os::Invalid_Handle;
		}

}

void Write_File(kiv_hal::TRegisters& regs) {
	kiv_os::THandle file_handle = regs.rdx.x;
	auto buffer = reinterpret_cast<char*>(regs.rdi.r);
	size_t size = regs.rcx.r;
	//std::lock_guard<std::mutex> lck_st(file_handles.table_mutex);
	const auto& file = file_handles.Get_File(file_handle);
	if (file) {
		
		size_t written;
		auto error = file->Write(buffer, size, written);
		regs.rax.r = written;

		if (error != kiv_os::NOS_Error::Success) {
			Set_Error(regs, error);
		}

	}
	else {
		Set_Error(regs, kiv_os::NOS_Error::File_Not_Found);
	}
}

void Read_File(kiv_hal::TRegisters& regs) {
	kiv_os::THandle file_handle = regs.rdx.x;
	auto buffer = reinterpret_cast<char*>(regs.rdi.r);
	size_t size = regs.rcx.r;

	//std::lock_guard<std::mutex> lck_st(file_handles.table_mutex);
	const auto& file = file_handles.Get_File(file_handle);
	if (file) {
		

		size_t read = 0;
		auto error = file->Read(buffer, size, read);
		regs.rax.r = read;

	}
	else {
		Set_Error(regs, kiv_os::NOS_Error::File_Not_Found);
	}


/*
	size_t read;
	auto error = file->Read(buffer, size, read);
	regs.rax.r = read;


	Set_Error(regs, error);
*/
}
const size_t PIPE_BUFFER_SIZE_BYTES = 1024;
void Create_Pipe(kiv_hal::TRegisters& regs) {
	std::lock_guard<std::mutex> lck_st(file_handles.table_mutex);

	std::shared_ptr<Pipe> pipe = std::make_shared<Pipe>(PIPE_BUFFER_SIZE_BYTES / sizeof(char));

	/*
	auto in = std::make_shared<PipeFileIn>(pipe);
	auto out = std::make_shared<PipeFileOut>(pipe);
	*/

	auto in = std::make_shared<PipeFile>(pipe);
	auto out = std::make_shared<PipeFile>(pipe);

	kiv_os::THandle in_handle = file_handles.Add_File(in);
	kiv_os::THandle out_handle = file_handles.Add_File(out);

	auto* arr = reinterpret_cast<kiv_os::THandle*>(regs.rdx.r);
	arr[0] = in_handle;
	arr[1] = out_handle;

}


void Close_IO(kiv_hal::TRegisters& regs) {
	std::lock_guard<std::mutex> lck_st(file_handles.table_mutex);
	kiv_os::THandle file_handle = regs.rdx.x;

	const std::shared_ptr<File>& file = file_handles.Get_File(file_handle);
	if (file != nullptr) {
		file->Close();
		file_handles.Delete_File(file_handle);
	}

	auto result = file_handles.Delete_File(file_handle);
}


void Close_Handle(kiv_hal::TRegisters& regs) {

	
	std::lock_guard<std::mutex> lck_st(file_handles.table_mutex);
	kiv_os::THandle file_handle = regs.rdx.x;
	if (file_handle > 2) {
		const std::shared_ptr<File>& file = file_handles.Get_File(file_handle);
		if (file != nullptr) {
			file->Close();
			file_handles.Delete_File(file_handle);
		}

		auto result = file_handles.Delete_File(file_handle);
	}
}

void Delete_File(kiv_hal::TRegisters& regs) {

	auto file_name = reinterpret_cast<const char*>(regs.rdx.r);
	
	std::filesystem::path path(file_name);

	if (!path.is_absolute() && !path.has_root_path()) {
		fs::path current = Process_working_Dir();
		path = current / path;
	}
	path = (path.root_path() / path.relative_path()).lexically_normal();


	auto error = Get_File_System()->Remove(path);

	if (error != kiv_os::NOS_Error::Success) {
		return;
	}

}

void Set_Working_Dir(kiv_hal::TRegisters& regs) {
	auto path_name = reinterpret_cast<const char*>(regs.rdx.r);
	kiv_os::NFile_Attributes attributes;

	std::filesystem::path path(path_name);

	if (!path.is_absolute() && !path.has_root_path()) {
		fs::path current = Process_working_Dir();
		path = current / path;
	}
	path = (path.root_path() / path.relative_path()).lexically_normal();

	auto error = Get_File_System()->Get_File_Attributes(path, attributes);
	if (!(static_cast<uint8_t>(attributes) & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory))) {
		Set_Error(regs, kiv_os::NOS_Error::Invalid_Argument);
		return;
	}

	Set_Process_Working_Dir(path);
}

void Get_Working_Dir(kiv_hal::TRegisters& regs) {
	char* out_buffer = reinterpret_cast<char*>(regs.rdx.r);
	size_t buffer_size = static_cast<size_t>(regs.rcx.r);
	//size_t written;
	
	auto working_dir = Process_working_Dir();

	strncpy_s(out_buffer, buffer_size, working_dir.c_str(), working_dir.size());
	regs.rax.r = strnlen(out_buffer, buffer_size - 1) + 1;
	
}

void Set_File_Attributes(kiv_hal::TRegisters& regs) {
	auto file_name = reinterpret_cast<const char*>(regs.rdx.r);
	kiv_os::NFile_Attributes attributes = static_cast<kiv_os::NFile_Attributes>(regs.rdi.i);

	std::filesystem::path path(file_name);

	if (!path.is_absolute() && !path.has_root_path()) {
		fs::path current = Process_working_Dir();
		path = current / path;
	}
	path = (path.root_path() / path.relative_path()).lexically_normal();

	auto error = Get_File_System()->Get_File_Attributes(path, attributes);

	if (error != kiv_os::NOS_Error::Success) {
		return;
	}
	
	

}

void Get_File_Attributes(kiv_hal::TRegisters& regs) {

	auto file_name = reinterpret_cast<const char*>(regs.rdx.r);
	kiv_os::NFile_Attributes attributes;

	std::filesystem::path path(file_name);

	if (!path.is_absolute() && !path.has_root_path()) {
		fs::path current = Process_working_Dir();
		path = current / path;
	}
	path = (path.root_path() / path.relative_path()).lexically_normal();

	auto error = Get_File_System()->Get_File_Attributes(path, attributes);

	if (error != kiv_os::NOS_Error::Success) {
		return;
	}
	regs.rdi.i = static_cast<uint8_t>(attributes);

}

void Handle_IO(kiv_hal::TRegisters& regs) {

	switch (static_cast<kiv_os::NOS_File_System>(regs.rax.l)) {

	case kiv_os::NOS_File_System::Open_File:
		Open_File(regs);
		break;
	case kiv_os::NOS_File_System::Write_File:
		Write_File(regs);
		break;
	case kiv_os::NOS_File_System::Read_File:
		Read_File(regs);
		break;
	case kiv_os::NOS_File_System::Create_Pipe:
		Create_Pipe(regs);
		break;
	case kiv_os::NOS_File_System::Close_Handle:
		Close_Handle(regs);
		break;
	case kiv_os::NOS_File_System::Delete_File:
		Delete_File(regs);
		break;
	case kiv_os::NOS_File_System::Set_Working_Dir:
		Set_Working_Dir(regs);
		break;
	case kiv_os::NOS_File_System::Get_Working_Dir:
		Get_Working_Dir(regs);
		break;
	case kiv_os::NOS_File_System::Set_File_Attribute:
		Set_File_Attributes(regs);
		break;
	case kiv_os::NOS_File_System::Get_File_Attribute:
		Get_File_Attributes(regs);
		break;
	}
}