#include <algorithm>
#include <iostream>
#include <vector>
#include "pipe.h"



size_t Pipe::Write(char* buffer, size_t size) {

    this->empty_semaphore.wait();
    for (size_t i = 0; i < size; ++i) {
        pipe_buffer.push_back(buffer[i]);
    }
    this->full_semaphore.notify();

    return size;
}

std::vector<char> Pipe::Read(size_t size, size_t& read) {

    pipe_buffer.reserve(size);
    std::vector<char> data;
    for (size_t i = 0; i < size; i++) {
        if (close_in) {
            data = pipe_buffer;
            read = data.size();
            return data;
        }
        this->full_semaphore.wait();

        if (size <= pipe_buffer.size()) {
            data.insert(data.begin(), pipe_buffer.begin(), pipe_buffer.begin() + size);
            pipe_buffer.erase(pipe_buffer.begin(), pipe_buffer.begin() + size);
            read = data.size();
            return data;
        }

       // this->full_semaphore.wait();
        //data = pipe_buffer;
        this->empty_semaphore.notify();
    }
    read = data.size();
    return data;
}


kiv_os::NOS_Error PipeFile::Write(char* buffer, size_t size, size_t& written) {

    written = pipe->Write(buffer, size);

    if (written > 0) {
        return kiv_os::NOS_Error::Success;
    }
    else {
        return kiv_os::NOS_Error::IO_Error;
    }
}

kiv_os::NOS_Error PipeFile::Read(char* buffer, size_t size, size_t& read) {

    auto data = pipe->Read(size, read);


    std::copy(data.begin(), data.end(), buffer);

    if (data.empty()) {
        return kiv_os::NOS_Error::IO_Error;
    }
    else {
        data.clear();
        return kiv_os::NOS_Error::Success;
    }
}

void PipeFile::Close() {
    this->pipe->Close();
}

PipeFile::PipeFile(std::shared_ptr<Pipe> p) : pipe(std::move(p)) {};

Pipe::Pipe(size_t capacity) :
    full_semaphore(0),
    empty_semaphore(capacity),
    capacity(capacity) {
}