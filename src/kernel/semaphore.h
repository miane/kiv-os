#pragma once

#include <mutex>
#include <condition_variable>

class Semaphore {
private:
    size_t count;
    std::condition_variable c;
    std::mutex mtx;

public:
    explicit Semaphore() : count(0) {};
    explicit Semaphore(size_t count) : count(count) {};

    void notify() {
        std::unique_lock<std::mutex> lock(mtx);
        count++;
        c.notify_one();
    }

    void wait() {
        std::unique_lock<std::mutex> lock(mtx);
        while (count == 0) {
            c.wait(lock);
        }
        count--;
    }
};

