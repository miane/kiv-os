#include "handles.h"

#include <map>
#include <mutex>
#include <random>
#include <set>
#include <condition_variable>

std::map<kiv_os::THandle, HANDLE> Handles;
std::mutex Handles_Guard;
kiv_os::THandle Last_Handle = 2;
std::set<kiv_os::THandle> Allocated_Handles;

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_int_distribution<> dis(1, 6);




kiv_os::THandle Alloc_Handle() {
	std::lock_guard<std::mutex> guard(Handles_Guard);

	/*if (Allocated_Handles.size() >= std::numeric_limits<kiv_os::THandle>::max()) {
		return kiv_os::Invalid_Handle;
	}*/

	do {
		Last_Handle += dis(gen);
	} while (Allocated_Handles.find(Last_Handle) != Allocated_Handles.end());

	Allocated_Handles.insert(Last_Handle);
	return Last_Handle;
}



kiv_os::THandle Convert_Native_Handle(const HANDLE hnd) {
	std::lock_guard<std::mutex> guard(Handles_Guard);

	Last_Handle += dis(gen);	//vygenerujeme novy interni handle s nahodnou hodnotou

	Handles.insert(std::pair<kiv_os::THandle, HANDLE>(Last_Handle, hnd));

	return Last_Handle;
}

HANDLE Resolve_kiv_os_Handle(const kiv_os::THandle hnd) {
	std::lock_guard<std::mutex> guard(Handles_Guard);

	auto resolved = Handles.find(hnd);
	if (resolved != Handles.end()) {
		return resolved->second;
	}
	else
		return INVALID_HANDLE_VALUE;
}


bool Remove_Handle(const kiv_os::THandle hnd) {
	std::lock_guard<std::mutex> guard(Handles_Guard);

	return Handles.erase(hnd) == 1;
}