#include "process.h"
#include "kernel.h"
#include <random>
#include <iostream>
#include <mutex>
#include <memory>
#include <thread>
#include <libloaderapi.h>
#include "handles.h"
#include <map>
#include "io.h"


Process_Control_Block PCB;
Thread_Control_Block TCB;
std::mutex Semaphores_Mutex;
std::recursive_mutex thread_table_mutex;
std::map<std::thread::id, kiv_os::THandle> std_thread_table;


std::string Process_working_Dir() {
    std::lock_guard<std::recursive_mutex> ttp_lck(thread_table_mutex);
    
    auto proc_handle = std_thread_table[std::this_thread::get_id()];
    auto process = PCB.Find_Process(proc_handle);
    
    if (process == nullptr) {
        return "C:\\";
    }
    
    return process.get()->working_directory;  
}

void Set_Process_Working_Dir(std::filesystem::path working_dir) {
    std::lock_guard<std::recursive_mutex> ttp_lck(thread_table_mutex);
    auto proc_handle = std_thread_table[std::this_thread::get_id()];
    auto p_process = PCB.Find_Process(proc_handle);

    if (p_process == nullptr) {
        return;
    }
    
    auto process = p_process.get();
    std::lock_guard<decltype(process->mutex)> lck(process->mutex);
    process->working_directory = working_dir.string();
    
}

kiv_os::THandle Get_Current_Process_Handle() {
    std::lock_guard<std::recursive_mutex> ttp_lck(thread_table_mutex);
     return std_thread_table[std::this_thread::get_id()];  
}

void Add_Owned_Handle(kiv_os::THandle process_handle, kiv_os::THandle file_handle) {
    auto p_process = PCB.Find_Process(process_handle);

    if (p_process == nullptr) {
        return;
    }

    p_process->owned_handles.insert(file_handle);   
}

Process_Control_Block* Get_PCB()
{
    return &PCB;
}

//------ for create process ---------------------------------------------------------------------

size_t __stdcall Default_Signal_Handler(const kiv_hal::TRegisters& regs) {
    auto signal_id = static_cast<kiv_os::NSignal_Id>(regs.rcx.l);
    return 0;
}

/*
void Thread_Execute(bool is_process) {

    std::lock_guard<std::mutex> guard(Semaphores_Mutex);

    kiv_os::THandle handle = std_thread_table[std::this_thread::get_id()];


    if (is_process) {
        auto process = PCB.Find_Process(handle);
        if (process) {
            process->status = Process_Status::Zombie;
        }
    }

    auto result = TCB.listeners.find(handle);
    Remove_Handle(handle);

    if (result != TCB.listeners.end()) {

        auto& listeners = TCB.listeners[handle];

        for (auto listener : listeners) {

            // notify the listeners of the thread
            listener.get()->source = handle;
            listener.get()->semaphore->notify();
        }

        listeners.clear();
        TCB.listeners.erase(handle);
    }

    if (TCB.thread_to_parent_process.find(handle) != TCB.thread_to_parent_process.end()) {
        TCB.thread_to_parent_process.erase(handle);
    }

    std_thread_table.erase(std::this_thread::get_id());
}


void Thread_Entrypoint(kiv_os::TThread_Proc threadproc, kiv_hal::TRegisters& regs, bool is_process,
    std::shared_ptr<Semaphore> semaphore) {

    semaphore->wait();
    //delete s;
    threadproc(regs);

    Thread_Execute(is_process);

}


kiv_hal::TRegisters Prepare_Process_Context(unsigned short std_in, unsigned short std_out, char* args) {
    kiv_hal::TRegisters regs{};
    regs.rax.x = std_in;
    regs.rbx.x = std_out;
    regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(args);
    return regs;
}

void run_in_a_thread(kiv_os::TThread_Proc t_threadproc, kiv_hal::TRegisters& registers, kiv_hal::TRegisters& program_registers, bool is_process, 
    std::shared_ptr<Semaphore> semaphore) {
    std::lock_guard<std::mutex> guard(Semaphores_Mutex);

    std::thread t1(Thread_Entrypoint, t_threadproc, program_registers, is_process, semaphore);

    HANDLE native_handle = t1.native_handle();
    kiv_os::THandle handle = Convert_Native_Handle(native_handle);
    std_thread_table[std::this_thread::get_id()] = handle;
    
    
    registers.rax.x = handle;  
    t1.detach();
}

//-----------------------------------------------------------------------------------------------

void Create_Process(kiv_hal::TRegisters& regs) {
    auto program_name = reinterpret_cast<char*>(regs.rdx.r);
    auto args = reinterpret_cast<char*>(regs.rdi.r);

    kiv_os::THandle std_in = (regs.rbx.e >> 16) & 0xFFFF;
    kiv_os::THandle std_out = regs.rbx.e & 0xFFFF;

    auto program_regs = Prepare_Process_Context(std_in, std_out, args);
    kiv_os::TThread_Proc program_main = reinterpret_cast<kiv_os::TThread_Proc>(GetProcAddress(User_Programs, program_name));

    if (!program_main) {
        Set_Error(regs, kiv_os::NOS_Error::Invalid_Argument);
        // Set_Error(regs, kiv_os::NOS_Error::File_Not_Found);
        return;
    }

    auto semaphore = std::make_shared<Semaphore>(0);

    run_in_a_thread(program_main, regs, program_regs, true, semaphore);

    auto proc_handle = Alloc_Handle();
    program_regs.rcx.x = regs.rax.x;
    if (regs.rax.x == kiv_os::Invalid_Handle) {
        Set_Error(regs, kiv_os::NOS_Error::Out_Of_Memory); 
        return;
    }


    TCB.listeners.insert(std::pair<kiv_os::THandle, std::vector<std::shared_ptr<Wait_For_Listener>>>(
        program_regs.rcx.x, std::vector<std::shared_ptr<Wait_For_Listener>>()));

    auto process = PCB.Add_Process(regs.rax.x, std_in, std_out, program_name);
    std::lock_guard<std::recursive_mutex> proc_lck(process->mutex);
    process->status = Process_Status::Running;
    process->working_directory = "C:\\";
    process->parent_process = std_thread_table[std::this_thread::get_id()];
    process->signal_handlers[kiv_os::NSignal_Id::Terminate] = Default_Signal_Handler;
    auto parent_process = PCB.Find_Process(process->parent_process);
    if (parent_process != nullptr) 
    {
         //= working_dir.string();
        auto parent_work_dir = parent_process->working_directory;
        process->working_directory = parent_work_dir;
    }
    //regs.rax.x = program_regs.rcx.x;
    semaphore ->notify();
}

void Create_Thread(kiv_hal::TRegisters& regs) {
    auto s = std::make_shared<Semaphore>(0);
    kiv_hal::TRegisters thread_regs{};
    thread_regs.rdi.r = regs.rdi.r;

    run_in_a_thread(((kiv_os::TThread_Proc)regs.rdx.r), regs, thread_regs, false, s);

    auto resolved = std_thread_table.find(std::this_thread::get_id());
    kiv_os::THandle thread_handle = regs.rax.x;

    if (resolved != std_thread_table.end()) {
        auto proc = PCB.Find_Process(std_thread_table[std::this_thread::get_id()]);
    
        if (proc != nullptr) {
        
            TCB.thread_to_parent_process[thread_handle] = proc.get()->handle;
        }
    }
    s->notify();  
}
*/
//-----------------------------------------------------------------------------------------------

void Thread_Entrypoint(kiv_os::TThread_Proc threadproc, kiv_hal::TRegisters& regs, bool is_process,
    std::shared_ptr<Semaphore> semaphore) {
    semaphore->wait();
    kiv_os::THandle handle = std_thread_table[std::this_thread::get_id()];
    size_t exit_code = threadproc(regs);
    //std::lock_guard<std::mutex> guard(Semaphores_Mutex);

    if (is_process) {
        auto process = PCB.Find_Process(handle);
        if (process) {
            process->exit_code = static_cast<kiv_os::NOS_Error>(exit_code);
            process->status = Process_Status::Zombie;
        }
    }
    else {
        auto parent_process_handle = TCB.thread_to_parent_process[handle];
        auto parent_process = PCB.Find_Process(parent_process_handle);
        if (parent_process) {
            auto thread = parent_process->threads[handle];
            thread->exit_code = static_cast<kiv_os::NOS_Error>(exit_code);
        }
    }

    Remove_Handle(handle);
    auto listeners = TCB.listeners.find(handle);
    if (listeners != TCB.listeners.end()) {
        for (auto& listener : listeners->second) {
            listener->source = handle;
            listener->semaphore->notify();
        }
    }
    
    TCB.listeners.erase(handle);
    std_thread_table.erase(std::this_thread::get_id());
}


kiv_hal::TRegisters Prepare_Process_Context(unsigned short std_in, unsigned short std_out, char* args) {
    kiv_hal::TRegisters regs{};
    regs.rax.x = std_in;
    regs.rbx.x = std_out;
    regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(args);
    return regs;
}


//-----------------------------------------------------------------------------------------------

void Create_Process(kiv_hal::TRegisters& regs) {
    auto program_name = reinterpret_cast<char*>(regs.rdx.r);
    auto args = reinterpret_cast<char*>(regs.rdi.r);

    kiv_os::THandle std_in = (regs.rbx.e >> 16) & 0xFFFF;
    kiv_os::THandle std_out = regs.rbx.e & 0xFFFF;

    kiv_os::TThread_Proc program_main = reinterpret_cast<kiv_os::TThread_Proc>(GetProcAddress(User_Programs, program_name));

    if (!program_main) {
        Set_Error(regs, kiv_os::NOS_Error::Invalid_Argument);
        // Set_Error(regs, kiv_os::NOS_Error::File_Not_Found);
        return;
    }

    auto program_regs = Prepare_Process_Context(std_in, std_out, args);
    //program_regs->rdi.r = regs.rdi.r;

    auto process_semaphore = std::make_shared<Semaphore>(0);
    //std::lock_guard<std::mutex> guard(Semaphores_Mutex);

    std::thread t(Thread_Entrypoint, program_main, program_regs, true, process_semaphore);
    HANDLE native_handle = t.native_handle();
    kiv_os::THandle handle = Convert_Native_Handle(native_handle);
    std_thread_table[t.get_id()] = handle;

    //program_regs->rcx.x = handle;
    program_regs.rcx.x = handle;


    TCB.listeners.insert(std::pair<kiv_os::THandle, std::vector<std::shared_ptr<Wait_For_Listener>>>(
        handle, std::vector<std::shared_ptr<Wait_For_Listener>>()));

    auto process = PCB.Add_Process(handle, std_in, std_out, program_name);
    process->status = Process_Status::Running;
    process->working_directory = "C:\\";
    process->parent_process = std_thread_table[std::this_thread::get_id()];
    process->signal_handlers[kiv_os::NSignal_Id::Terminate] = Default_Signal_Handler;
    auto parent_process = PCB.Find_Process(process->parent_process);
    if (parent_process != nullptr)
    {
        //= working_dir.string();
        auto parent_work_dir = parent_process->working_directory;
        process->working_directory = parent_work_dir;
    }

    //regs.rax.x = program_regs->rcx.x;
    regs.rax.x = program_regs.rcx.x;

    process_semaphore->notify();
    t.detach();
}

void Create_Thread(kiv_hal::TRegisters& regs) {

    std::lock_guard<decltype(PCB.pcb_mutex)> pt_lck(PCB.pcb_mutex);
  

    kiv_os::TThread_Proc thread_main = reinterpret_cast<kiv_os::TThread_Proc>(regs.rdx.r);
    if (!thread_main) {
        Set_Error(regs, kiv_os::NOS_Error::File_Not_Found); //opravit
        return;
    }

    auto parent_process_handle = std_thread_table[std::this_thread::get_id()];
    auto parent_process = PCB.Find_Process(parent_process_handle);

    //auto thread_regs = std::make_shared<kiv_hal::TRegisters>();
    //thread_regs->rdi.r = regs.rdi.r;
    kiv_hal::TRegisters thread_regs{};
    thread_regs.rdi.r = regs.rdi.r;

    auto thread_semaphore = std::make_shared<Semaphore>(0);
    std::shared_ptr<Thread> thread = std::make_shared<Thread>();
    thread->thread = std::thread(Thread_Entrypoint, thread_main, thread_regs, false, thread_semaphore);
    thread->handle = Convert_Native_Handle(thread->thread.native_handle());

    //thread_regs->rcx.x = thread->handle;
    thread_regs.rcx.x = thread->handle;

    TCB.listeners.insert(std::pair<kiv_os::THandle, std::vector<std::shared_ptr<Wait_For_Listener>>>(
        thread->handle, std::vector<std::shared_ptr<Wait_For_Listener>>()));
    std_thread_table[thread->thread.get_id()] = thread->handle;

    parent_process->threads.insert(std::pair<kiv_os::THandle, std::shared_ptr<Thread>>(thread->handle, thread));
    TCB.thread_to_parent_process.insert(std::pair<kiv_os::THandle, kiv_os::THandle>(thread->handle, parent_process_handle));

    //regs.rax.x = thread_regs->rcx.x
    regs.rax.x = thread_regs.rcx.x;

    thread_semaphore->notify();
    thread->thread.detach();
}


void Handle_Clone(kiv_hal::TRegisters& regs) {
    switch (static_cast<kiv_os::NClone>(regs.rcx.r)) {
    case kiv_os::NClone::Create_Process:
        Create_Process(regs);
        break;
    case kiv_os::NClone::Create_Thread:
        Create_Thread(regs);
        break;
    }
}

void Handle_Wait_For(kiv_hal::TRegisters& regs) {
    auto handles = reinterpret_cast<kiv_os::THandle*>(regs.rdx.r);
    auto handles_size = regs.rcx.l;

  //  std::unique_lock<std::mutex> lock(Semaphores_Mutex);
    auto listener = std::make_shared<Wait_For_Listener>(std::make_unique<Semaphore>(0), -1);
    bool handles_found = false;

    // Add listeners
    for (int i = 0; i < handles_size; i++) {
        TCB.listeners[handles[i]].emplace_back(listener);
    }
    
      //  lock.unlock();
        // Wait for listeners
        listener->semaphore->wait();
       // lock.lock();

       /* 
    for (int i = 0; i < handles_size; i++) {

        auto handle = handles[i];
        auto found = TCB.listeners.find(handle);
        if (found == TCB.listeners.end()) {
            handles_found = false;
         
        }
        else {
            found->second.push_back(listener);
           
        }
    }
    */
    regs.rax.l = listener->source;

}

void Handle_Read_Exit_Code(kiv_hal::TRegisters& regs) {
    auto handle = kiv_os::THandle(regs.rdx.x);
    
   
    kiv_hal::TRegisters wait_for_regs{};
    wait_for_regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(&handle);
    wait_for_regs.rcx.l = 1;

    Handle_Wait_For(wait_for_regs);



    if (auto process = PCB.Find_Process(handle)) {
        regs.rcx.x = static_cast<decltype(regs.rcx.x)>(process->exit_code);
        PCB.Remove_Process(handle);
    }
    else {
        auto parent_handle = TCB.thread_to_parent_process.find(handle);
        if (parent_handle != TCB.thread_to_parent_process.end()) {
            auto parent_process = PCB.Find_Process(parent_handle->second);
            auto handle = parent_process.get()->handle;  ////find(handle);
           // if (thread != parent_process->threads.end()) {
                regs.rcx.x = static_cast<decltype(regs.rcx.x)>(parent_process.get()->exit_code);
                //parent_process->threads.erase(handle);
            //}
        }
        else {
            Set_Error(regs, kiv_os::NOS_Error::File_Not_Found); // opravit
        }
    }
    
/*
    kiv_os::NOS_Error exit_code = kiv_os::NOS_Error::Unknown_Error;
    
    std::lock_guard<std::mutex> guard(Semaphores_Mutex);

    if (auto process = PCB.Find_Process(handle)) {
       // process->status == Process_Status::Zombie
        exit_code = process->exit_code;
       
        PCB.Remove_Process(handle);
    }
    regs.rcx.x = static_cast<decltype(regs.rcx.x)>(exit_code);
    */
}

void Handle_Exit(kiv_hal::TRegisters& regs) {
   
    auto exit_code = static_cast<kiv_os::NOS_Error>(regs.rcx.x);
    auto handle = std_thread_table[std::this_thread::get_id()];

    auto tab = TCB.thread_to_parent_process.find(handle);
    if (tab != TCB.thread_to_parent_process.end()) {
        // exit with the given exit code
        tab->second;

    }
    else {

        handle = kiv_os::Invalid_Handle;
    }
    auto process = PCB.Find_Process(handle);
    //auto exit_code = static_cast<kiv_os::NOS_Error>(regs.rcx.x);
    if (process != nullptr) {
        // set the exit code
        //process->exit_code = exit_code;
        // set the status to Zombie
        process->status = Process_Status::Zombie;
    }
    
    ExitThread(0);
}

//-----------------------------------------------------------------------------------

void Signal(kiv_os::NSignal_Id signal_id, const std::shared_ptr<Process> process) {
    

    auto signal_handler = process->signal_handlers.find(signal_id);
    if (signal_handler != process->signal_handlers.end()) {
        kiv_hal::TRegisters regs{};
        regs.rcx.l = static_cast<decltype(regs.rcx.l)>(signal_id);
        signal_handler->second(regs);
    }
}

void Signal_All_Processes(kiv_os::NSignal_Id signal_id) {
    // iterate over all processes and signal the given signal
    for (const auto process : PCB.Get_Processes()) {
        Signal(signal_id, process);
    }
}

void Shutdown_All_Processes() {

    Signal_All_Processes(kiv_os::NSignal_Id::Terminate);

    for (const auto process : PCB.Get_Processes()) {
        kiv_hal::TRegisters regs{};
        regs.rdx.x = static_cast<decltype(regs.rdx.x)>(process->std_in);
        Close_IO(regs);
    }
}

void Handle_Shutdown(kiv_hal::TRegisters& regs) {
    Shutdown_All_Processes();
}

//---------------------------------------------------------------------------------

void Handle_Register_Signal_Handler(kiv_hal::TRegisters& regs) {

    kiv_os::THandle handle = std_thread_table[std::this_thread::get_id()];
    
    kiv_os::THandle a = Get_Current_Process_Handle();

    auto tab = TCB.thread_to_parent_process.find(handle);
    if (tab != TCB.thread_to_parent_process.end()) {
        // exit with the given exit code
        tab->second;

    }
    else {

        handle = kiv_os::Invalid_Handle;
    }
    auto process_id = PCB.Find_Process(handle);
    auto signal = static_cast<kiv_os::NSignal_Id>(regs.rcx.r);
    if (regs.rdx.r == 0) {
        process_id->signal_handlers.erase(signal);
    }
    else {
        // registering
        process_id->signal_handlers[signal] = reinterpret_cast<kiv_os::TThread_Proc>(regs.rdx.r);
    }
}

// ------------------------------------------------------------------------------


void Handle_Process(kiv_hal::TRegisters& regs) {
    switch (static_cast<kiv_os::NOS_Process>(regs.rax.l)) {
    case kiv_os::NOS_Process::Clone:
        Handle_Clone(regs);
        break;

    case kiv_os::NOS_Process::Wait_For:
        Handle_Wait_For(regs);
        break;

    case kiv_os::NOS_Process::Read_Exit_Code:
        Handle_Read_Exit_Code(regs);
        break;

    case kiv_os::NOS_Process::Exit:
        Handle_Exit(regs);
        break;

    case kiv_os::NOS_Process::Shutdown:
        Handle_Shutdown(regs);
        break;
    case kiv_os::NOS_Process::Register_Signal_Handler:
        Handle_Register_Signal_Handler(regs);
        break;
    }
}

