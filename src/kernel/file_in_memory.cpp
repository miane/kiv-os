#include "file_in_memory.h"
#include "files.h"


File_In_Memory::File_In_Memory(size_t size)
    : area(new char[size]), area_size(size)
{
    // nothing more to do
}

File_In_Memory::File_In_Memory(void* src_buffer, size_t size)
    : File_In_Memory(size)
{
    memcpy(area, src_buffer, size);
}

kiv_os::NOS_Error File_In_Memory::Write(char* buffer, size_t size, size_t& written) {
    std::lock_guard<decltype(mutex)> lck(mutex);

    size_t remaining = area_size - position;
    size_t to_write = (size > remaining) ? remaining : size;

    memcpy(reinterpret_cast<void*>(reinterpret_cast<size_t>(area) + position), buffer, to_write);
    position += to_write;
    written = to_write;

    return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error File_In_Memory::Read(char* buffer, size_t size, size_t& read) {
    std::lock_guard<decltype(mutex)> lck(mutex);

    size_t remaining = area_size - position;
    size_t to_read = (size > remaining) ? remaining : size;

    memcpy(buffer, reinterpret_cast<char*>(area) + position, to_read);

    position += to_read;
    read = to_read;

    return kiv_os::NOS_Error::Success;
}

File_In_Memory::~File_In_Memory() {
    delete area;
}