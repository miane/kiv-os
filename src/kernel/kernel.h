#pragma once

#include<Windows.h>
#include "..\api\hal.h"
#include "..\api\api.h"
#include "vfs.h"

extern HMODULE User_Programs;

void Set_Error(kiv_hal::TRegisters& regs, kiv_os::NOS_Error error);
void __stdcall Bootstrap_Loader(kiv_hal::TRegisters &context);
File_System *Get_File_System();
