#pragma once

#include "kernel.h"
#include "io.h"
#include "process.h"

#include <Windows.h>
#include <iostream>

HMODULE User_Programs;

File_System *file_system = new File_System();
//void run_shell(const kiv_hal::TRegisters& regs);

void Initialize_Kernel() {
	Initialize_IO();
	User_Programs = LoadLibraryW(L"user.dll");
	file_system->Init();
}


void Shutdown_Kernel() {
	FreeLibrary(User_Programs);
	delete file_system;
}

void __stdcall Sys_Call(kiv_hal::TRegisters& regs) {

	switch (static_cast<kiv_os::NOS_Service_Major>(regs.rax.h)) {

	case kiv_os::NOS_Service_Major::File_System:
		Handle_IO(regs);
		break;
	case kiv_os::NOS_Service_Major::Process:
		Handle_Process(regs);
		break;
	}
}



void __stdcall Bootstrap_Loader(kiv_hal::TRegisters& context) {
	Initialize_Kernel();
	kiv_hal::Set_Interrupt_Handler(kiv_os::System_Int_Number, Sys_Call);
	kiv_hal::TRegisters regs{};
	//run_shell(regs);

	auto std_in = Get_Console_In();
	auto std_out = Get_Console_Out();

	std::string program_name = "shell";
	std::string args = "";
	kiv_hal::TRegisters shell_regs{};
	shell_regs.rbx.e = (std_in << 16) | std_out;
	shell_regs.rdx.r = reinterpret_cast<decltype(shell_regs.rdx.r)>(program_name.data());
	shell_regs.rdi.r = reinterpret_cast<decltype(shell_regs.rdi.r)>(args.data());

	shell_regs.rax.h = static_cast<decltype(shell_regs.rax.h)>(kiv_os::NOS_Service_Major::Process);
	shell_regs.rax.l = static_cast<decltype(shell_regs.rax.l)>(kiv_os::NOS_Process::Clone);
	shell_regs.rcx.l = static_cast<decltype(shell_regs.rcx.l)>(kiv_os::NClone::Create_Process);
	Sys_Call(shell_regs);

	kiv_hal::TRegisters exit_regs{};
	exit_regs.rax.h = static_cast<decltype(shell_regs.rax.h)>(kiv_os::NOS_Service_Major::Process);
	exit_regs.rax.l = static_cast<decltype(shell_regs.rax.l)>(kiv_os::NOS_Process::Read_Exit_Code);
	exit_regs.rbx.l = static_cast<decltype(exit_regs.rbx.l)>(kiv_os::NSignal_Id::Terminate);
	exit_regs.rdx.x = shell_regs.rax.x; // target process handle

	Sys_Call(exit_regs);


	Shutdown_Kernel();
}
/*
void run_shell(const kiv_hal::TRegisters& regs) {
	kiv_hal::TRegisters shell_regs{};

	auto std_in = Get_Console_In();
	auto std_out = Get_Console_Out();

	shell_regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>("shell");
	shell_regs.rdi.r = reinterpret_cast<decltype(regs.rdx.r)>("");
	shell_regs.rbx.e = (std_in << 16) | std_out;
	shell_regs.rcx.l = static_cast<uint8_t>(kiv_os::NClone::Create_Process);
	Create_Process(shell_regs);

	// prepare read exit code regs
	kiv_hal::TRegisters read_exit_code_regs{};
	read_exit_code_regs.rdx.x = static_cast<decltype(regs.rdx.x)>((kiv_os::THandle)shell_regs.rax.x);

	Handle_Read_Exit_Code(read_exit_code_regs);

	kiv_hal::TRegisters close_regs{};
	// close shell's std_out
	close_regs.rdx.x = std_out;
	Close_Handle(close_regs);

	// close shell's std_in
	close_regs.rdx.x = std_in;
	Close_Handle(close_regs);
}
*/

void Set_Error(kiv_hal::TRegisters& regs, kiv_os::NOS_Error error) {
	regs.flags.carry = true;
	regs.rax.r = static_cast<uint16_t>(error);
}


File_System *Get_File_System() {
	return file_system;
}
