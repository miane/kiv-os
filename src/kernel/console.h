#pragma once

#include "files.h"

class Console : public File {
    std::recursive_mutex mutex;

public:
    kiv_os::NOS_Error Write(char* buffer, size_t size, size_t& written);

    kiv_os::NOS_Error Read(char* buffer, size_t size, size_t& read);
};
