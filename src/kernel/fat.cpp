#include "fat.h"

#include <vector>


/**********************************************************************/
// Disk 

bool Write_To_Disk(uint8_t disk_num, kiv_hal::TDisk_Address_Packet disk_sector) {

    kiv_hal::TRegisters regs;
    kiv_hal::NDisk_Status status;

    regs.rdx.l = disk_num;
    regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Write_Sectors);
    regs.rdi.r = reinterpret_cast<uint64_t>(&disk_sector);

    kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);
    status = static_cast<kiv_hal::NDisk_Status>(regs.rax.x);
    return !regs.flags.carry;

}

bool Read_From_Disk(uint8_t disk_num, kiv_hal::TDisk_Address_Packet &disk_sector) {

    kiv_hal::TRegisters regs;
    kiv_hal::NDisk_Status status;

    regs.rdx.l = disk_num;
    regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Read_Sectors);
    regs.rdi.r = reinterpret_cast<uint64_t>(&disk_sector);


    kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);
    status = static_cast<kiv_hal::NDisk_Status>(regs.rax.x);
    return !regs.flags.carry;
}

bool Read_Disk(uint64_t sectors, uint64_t first_cluster, char* buffer, uint8_t disk_number) {
    
   
    kiv_hal::TDisk_Address_Packet disk_sector;  //struct from hal.h

    disk_sector.count = sectors;                 //number of sectors
    disk_sector.sectors = buffer;             //pointer to sectors buffer
    disk_sector.lba_index = first_cluster;      //adress first disk sector

    if (!Read_From_Disk(disk_number, disk_sector)) {
        return false;
    }
    
    return true;
    


}

bool Write_Disk(uint16_t sectors, uint64_t first_cluster, char* buffer, uint8_t disk_number) {

    kiv_hal::TDisk_Address_Packet disk_sector;			//struct from hal.h

    disk_sector.lba_index = first_cluster;              //adress first disk sector
    disk_sector.count = sectors;						//number of sectors
    disk_sector.sectors = buffer;						//pointer to sectors buffer

    if (!Write_To_Disk(disk_number, disk_sector)){
        return false;
    }

    return true;

};
/**********************************************************************/
//Table 

inline std::vector<uint8_t> Convert_To_Short(std::vector<uint16_t> converted_fat) {

    std::vector<uint8_t> vector;

    for (size_t i = 0; i < converted_fat.size(); i += 2) {

        char convert_buffer[8]; // 24 bits + stop znak      xyz XYZ - > yz Zx XY 
        char first[3];			// 8 bits + stop znak		yz	
        char second[3];			// 8 bits + stop znak		Zx
        char third[3];			// 8 bits + stop znak		XY
        size_t pos = i;
        snprintf(convert_buffer, sizeof(convert_buffer), "%.3X%.3X%", converted_fat.at(pos), converted_fat.at(pos + 1));  //parse to char buffer
        snprintf(first, sizeof(first), "%c%c", convert_buffer[1], convert_buffer[2]);
        snprintf(second, sizeof(second), "%c%c", convert_buffer[5], convert_buffer[0]);
        snprintf(third, sizeof(second), "%c%c", convert_buffer[3], convert_buffer[4]);

        vector.push_back(strtol(first, NULL, 16));
        vector.push_back(strtol(second, NULL, 16));
        vector.push_back(strtol(third, NULL, 16));

    }
    return vector;
}

std::vector<uint16_t> Convert_To_Long(std::vector<uint8_t> unconverted_fat) {
    std::vector<uint16_t> vector;

    for (int i = 0; i < unconverted_fat.size() - 1; i += 3) {

        char convert_buffer[7]; // 24 bits + stop znak      yz Zx XY - > xyz XYZ 
        char first[4];			// 12 bits + stop znak		xyz	
        char second[4];			// 12 bits + stop znak		XYZ

        size_t pos = i;
        snprintf(convert_buffer, sizeof(convert_buffer), "%.2X%.2X%.2X", unconverted_fat.at(i), unconverted_fat.at(pos + 1), unconverted_fat.at(pos + 2));  //parse to char buffer
        snprintf(first, sizeof(first), "%c%c%c", convert_buffer[3], convert_buffer[0], convert_buffer[1]);
        snprintf(second, sizeof(second), "%c%c%c", convert_buffer[4], convert_buffer[5], convert_buffer[2]);

        vector.push_back(strtol(first, NULL, 16));
        vector.push_back(strtol(second, NULL, 16));
    }
    return vector;
}

inline bool Fat_Tabs_Consystency(std::vector<uint8_t> first_tab, std::vector<uint8_t> second_tab) {
    if (first_tab.size() != second_tab.size()) {
        return false;
    }

    for (int i = 0; i < static_cast<int>(first_tab.size()); i++) {
        if (first_tab[i] != second_tab[i]) {
            return false;
            break;
        }
    }

    return true;
};

// when a some change in "uint16_t fat" -> update fats 
bool Update_Tabs(uint8_t disk_number, std::vector<uint16_t>& fat_table) {
    std::vector<uint8_t> table = Convert_To_Short(fat_table);

    for (size_t i = 0; i < 2; i++) {
        
        if (!Write_Disk(SECTORS_IN_FAT, 1 + i * 9, reinterpret_cast<char*>(table.data()), disk_number)) {
            return false;
        }
    }
    return true;

}

kiv_os::NOS_Error Load(uint8_t disk_number, kiv_hal::TDrive_Parameters& params, Boot_Sector& boot_sector, std::vector <uint16_t>& fat_table) {
    uint64_t count = (sizeof(Boot_Sector) + params.bytes_per_sector - 1) / params.bytes_per_sector;
    std::vector <uint16_t> buffer;
    buffer.resize((count * params.bytes_per_sector) / sizeof(uint16_t), 0);
    // read boot record
    if(!Read_Disk(count, 0, reinterpret_cast<char*>(buffer.data()), disk_number)) {
        return kiv_os::NOS_Error::IO_Error;
    }
    size_t offset = 11;
    uint8_t *temp = reinterpret_cast<uint8_t *>(buffer.data()) + offset;
    boot_sector = *reinterpret_cast<Boot_Sector *>(temp);
    
    std::vector <uint8_t> fat_buffer;
    
    fat_buffer.resize((static_cast<int64_t>(SECTORS_IN_FAT) * SECTOR_SIZE) / sizeof(char), 0);
    if (!Read_Disk(SECTORS_IN_FAT, 1, reinterpret_cast<char*>(fat_buffer.data()), disk_number)) {
        return kiv_os::NOS_Error::IO_Error;
    }
    
    fat_table = Convert_To_Long(fat_buffer);
    return kiv_os::NOS_Error::Success;
}

/**********************************************************************/

inline void Remove_Directory_Entry(Directory_Entry &entry) {
    std::memset(entry.name, 0, 8);
    std::memset(entry.extension, 0, 3);
    entry.attributes = 0;
    std::memset(entry.reserved, 0, 10);
    entry.last_write_time = 0;
    entry.last_write_date = 0;
    entry.first_logical_cluster = 0;
    entry.file_size = 0;
}

// empty dir check
bool is_Dir_Empty(const std::vector<Directory_Entry> items) {
    for (auto& item : items) {
        if (strncmp(item.name, ".       ", 8) == 0) {
            continue;
        }
        if (strncmp(item.name, "..      ", 8) == 0) {
            continue;
        }
        if (item.name[0] == '\0') {
            continue;
        }
        return false;
    }
    return true;
}

//root
bool is_Root(fs::path& path) {
    return path.relative_path().empty();
}

// find first free cluster
uint16_t Free_Cluster_Index(std::vector<uint16_t> fat_table) {

    uint16_t free_clust_num;

    for (int i = ADDRESS_START; i < ADDRESS_END; i++) {
        if (fat_table.at(i) == 0) {   //find free cluster -> break
            free_clust_num = i;
            break;
        }
        else free_clust_num = 0;
    }
    return free_clust_num;
}

//return vector path item without root
std::vector<std::string> Relativ_Path_Content(fs::path& path) {
    auto path_relative = path.relative_path();
    std::vector<std::string> path_part;
    for (auto it = path_relative.begin(); it != path_relative.end(); ++it) {
        if(sizeof(it->string()) > 0 ) path_part.push_back(it->string());
    }
    return path_part;
}       

// return items in root
std::vector<Directory_Entry> Read_Root(uint8_t disk_number) {
        
    std::vector<Directory_Entry> entries;

    std::vector<char> root_dir_data;
    root_dir_data.resize(SECTORS_IN_ROOT * SECTOR_SIZE, 0);
    Read_Disk(SECTORS_IN_ROOT, 19, root_dir_data.data(), disk_number);
    
    Directory_Entry* root_dir = reinterpret_cast<Directory_Entry*> (root_dir_data.data());

    for (int i = 0; i < ROOT_ENTRIES; i++) {

        entries.push_back(root_dir[i]);
    }
    return entries;
}

// items in othrer folder
kiv_os::NOS_Error Read_Directory(Directory_Entry& current_dir, uint8_t disk_number, std::vector<uint16_t>& fat_table, std::vector<Directory_Entry>& entries) {
    
    if (current_dir.first_logical_cluster == 0) { // is root dir
        entries = Read_Root(disk_number);
        return kiv_os::NOS_Error::Success;
    }
    
    
    std::vector<size_t> sector_list = Clusters_List(fat_table, current_dir.first_logical_cluster);

    std::vector<char> buffer;
    buffer.resize(SECTOR_SIZE, 0);

    const size_t ent_size = (sector_list.size() * SECTOR_SIZE) / sizeof(Directory_Entry);
    entries.reserve(ent_size);

    for (int i = 0; i < sector_list.size(); i++) {
        Read_Disk(1, static_cast<size_t>(sector_list[i]) + 31, buffer.data(), disk_number);
        Directory_Entry* dir_items = reinterpret_cast<Directory_Entry*>(buffer.data());
        for (int i = 0; i < 16; i++) {
            entries.push_back(dir_items[i]);
        }
    }

    return kiv_os::NOS_Error::Success;
}

//return name with dot and extension
std::string Get_Name(Directory_Entry& item) {
    std::string concat = "";
    char b = ' ';
    char c = '\0';
    size_t pos = 8;
    for (int i = 0; i < 8; i++) {
        char n = item.name[i];
        
        if (n == b || n == c) {
            pos = i;
            break;
        }
    }

    concat.append(item.name, 0, (pos));
    char ext = item.extension[0];
    if (ext == b || ext == c) {
        return concat;
    }
    concat.append(1u, '.');
    concat.append(item.extension, 0, 3);
    return concat;
}

// finds a first_logical_cluster of items and return / if doesn't find it -> -1  
int Item_Exist(std::string folder, std::vector<Directory_Entry> folder_cont, Directory_Entry& dir_item, int dir_num) {

    int i = 0;
    while (dir_num == -1 && i < folder_cont.size()) {
        dir_item = folder_cont.at(i);
        std::string item_to_check = Get_Name(dir_item);
        if (folder.compare(item_to_check) == 0) {
            dir_num = i;
        }
        i++;
    }
    return dir_num;
}

std::vector<size_t> Clusters_List(std::vector<uint16_t>& fat_table, uint16_t first_cluster) {

    std::vector<size_t> sector_list;
    size_t cluster = first_cluster;
    while (cluster < LAST_CLUSTER_START) {
        if (sector_list.size() == 0) { //first to list
            sector_list.push_back(cluster);

            cluster = fat_table[cluster];
            continue;
        }
        sector_list.push_back(cluster);
        cluster = fat_table[cluster];
    }
    return sector_list;
}


uint64_t Allocate_New_Cluster(uint8_t disk_number, uint16_t start_cluster, std::vector<uint16_t> fat_table) {
    
    auto free_index = Free_Cluster_Index(fat_table);

    if (free_index == 0) {
        return 0;
    }
    else { 
        auto cluster_list = Clusters_List(fat_table, start_cluster);         
        fat_table.at(free_index) = 4095;
        fat_table.at(cluster_list.at(cluster_list.size() - 1)) = free_index;

        Update_Tabs(disk_number, fat_table);
        return free_index;
    }
}

uint16_t Clusters_Count(std::vector<uint16_t>& fat_table, uint16_t first_cluster) {
    uint16_t count = 0;
    uint16_t cluster = first_cluster;
    while (cluster < LAST_CLUSTER_START) {
        count++;
        cluster = fat_table[cluster];
    }
    return count;
}
/**********************************************************************/

// delete folder content > "." and ".." and write to disk
kiv_os::NOS_Error Delete_Folder_Content(uint8_t disk_number, std::vector<Directory_Entry>& dir_cont, Directory_Entry& item) {

    for (auto& i : dir_cont) {
        if (strncmp(i.name, ".       ", 8) == 0) {
            Remove_Directory_Entry(i);
        }
        if (strncmp(i.name, "..      ", 8) == 0) {
            Remove_Directory_Entry(i);
            //std::memcpy(cont.data() + dir_num, &item, sizeof(Directory_Entry));
        }
    }
 
    if (!Write_Disk(1, FIRST_DATA_SEC + item.first_logical_cluster, reinterpret_cast<char*>(dir_cont.data()), disk_number)) {
        return kiv_os::NOS_Error::IO_Error;
    }

    return kiv_os::NOS_Error::Success;
}

// delete current folder content and write to disk
kiv_os::NOS_Error Delete_Folder(uint8_t disk_number, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table) {
    std::vector<Directory_Entry> cont;

    if (parent_dir.first_logical_cluster == 0) { // is root dir
        cont = Read_Root(disk_number);
        Directory_Entry dir_item;

        int dir_num = -1;
        dir_num = Item_Exist(Get_Name(item), cont, dir_item, dir_num);
        if (dir_num == -1)
        {
            return kiv_os::NOS_Error::File_Not_Found;
        }

        Remove_Directory_Entry(item);

        std::memcpy(cont.data() + dir_num, &item, sizeof(Directory_Entry));

        if (!Write_Disk(SECTORS_IN_ROOT, 19, reinterpret_cast<char*>(cont.data()), disk_number)) {
            return kiv_os::NOS_Error::IO_Error;
        }
        else {
            return kiv_os::NOS_Error::Success;
        }
    }
    else
    {
        size_t cluster_count = Clusters_Count(fat_table, parent_dir.first_logical_cluster);
        size_t max = (cluster_count * SECTOR_SIZE) / sizeof(Directory_Entry);
        Read_Directory(parent_dir, disk_number, fat_table, cont);
        int dir_num = -1;
        Directory_Entry dir_item;
        dir_num = Item_Exist(Get_Name(item), cont, dir_item, dir_num);
        if (dir_num == -1)
        {
            return kiv_os::NOS_Error::File_Not_Found;
        }

        Remove_Directory_Entry(item);

        std::memcpy(cont.data() + dir_num, &item, sizeof(Directory_Entry));
        if (!Write_Disk(static_cast<uint16_t>(cluster_count), FIRST_DATA_SEC + parent_dir.first_logical_cluster, reinterpret_cast<char*>(cont.data()), disk_number)) {
            return kiv_os::NOS_Error::IO_Error;
        }

    }

    return kiv_os::NOS_Error::Success;

}

kiv_os::NOS_Error Create_Folder(uint8_t disk_number, uint8_t attributes, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table) {
    
    std::vector<Directory_Entry> cont;
    int empty = -1;

    if (parent_dir.first_logical_cluster == 0) { // is root dir
        cont = Read_Root(disk_number);
        //Directory_Entry dir_item;
        for (int i = 0; i < ROOT_ENTRIES; i++) {
            if (cont[i].name[0] == 0 && empty == -1) {
                empty = i;
            }
        }
        if (empty != -1) { // is root dir
            std::memcpy(cont.data() + empty, &item, sizeof(Directory_Entry));
            if (!Write_Disk(SECTORS_IN_ROOT, 19, reinterpret_cast<char*>(cont.data()), disk_number)) {
                return kiv_os::NOS_Error::IO_Error;
            }
            return kiv_os::NOS_Error::Success;
        }
    }
    
        uint16_t cluster_count = Clusters_Count(fat_table, parent_dir.first_logical_cluster);
        uint16_t max = (cluster_count * SECTOR_SIZE) / sizeof(Directory_Entry);
        Read_Directory (parent_dir, disk_number, fat_table, cont);
        for (int i = 0; i < max; i++) {
            if (cont[i].name[0] == 0 && empty == -1) {
                empty = i;
            }
        }
        std::memcpy(cont.data() + empty, &item, sizeof(Directory_Entry));
        if (!Write_Disk(cluster_count, FIRST_DATA_SEC + parent_dir.first_logical_cluster, reinterpret_cast<char*>(cont.data()), disk_number)) {
            return kiv_os::NOS_Error::IO_Error;
        }

  

    return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Create_File(uint8_t disk_number, uint8_t attributes, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table) {


    int free_index = Free_Cluster_Index(fat_table);
    if (free_index == 0) 
    {
        return kiv_os::NOS_Error::Not_Enough_Disk_Space;
    }

    std::vector<Directory_Entry> cont;
    int empty = -1;

    if (parent_dir.first_logical_cluster == 0) { // is root dir
        cont = Read_Root(disk_number);
        //Directory_Entry dir_item;
        for (int i = 0; i < ROOT_ENTRIES; i++) {
            if (cont[i].name[0] == 0 && empty == -1) {
                empty = i;
            }
        }
        if (empty != -1) { 
            std::memcpy(cont.data() + empty, &item, sizeof(Directory_Entry));
            if (!Write_Disk(SECTORS_IN_ROOT, 19, reinterpret_cast<char*>(cont.data()), disk_number)) {
                return kiv_os::NOS_Error::IO_Error;
            }
           
          
            // save fat table where free cluster is marked occupied
            fat_table.at(free_index) = 4095; // mark of folder end destination
            if (!Update_Tabs(disk_number, fat_table))
            {
                return kiv_os::NOS_Error::Directory_Not_Empty;
            }
        }
        else {
        
            return kiv_os::NOS_Error::Directory_Not_Empty;
        }
    }

    uint16_t cluster_count = Clusters_Count(fat_table, parent_dir.first_logical_cluster);
    uint16_t max = (cluster_count * SECTOR_SIZE) / sizeof(Directory_Entry);
    Read_Directory(parent_dir, disk_number, fat_table, cont);
    for (int i = 0; i < max; i++) {
        if (cont[i].name[0] == 0 && empty == -1) {
            empty = i;
        }
    }
    std::memcpy(cont.data() + empty, &item, sizeof(Directory_Entry));
    if (!Write_Disk(cluster_count, FIRST_DATA_SEC + parent_dir.first_logical_cluster, reinterpret_cast<char*>(cont.data()), disk_number)) {
        return kiv_os::NOS_Error::IO_Error;
    }



    return kiv_os::NOS_Error::Success;
}

// find item and item's parent dir -> if something dont exist, parent is last exist item in path
kiv_os::NOS_Error Find(fs::path& path, uint8_t disk_number, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table) {

    if (is_Root(path)) {
        {
            //found item is parent
            std::memset(item.name, 0, 8);
            std::memset(item.extension, 0, 3);
            item.attributes = static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory);
            item.first_logical_cluster = 0;
            item.file_size = 0;
        }
        return kiv_os::NOS_Error::Success;
    }
    
    path = path.has_filename() ? path : path.parent_path();
    std::vector<std::string> path_part = Relativ_Path_Content(path);

    std::vector<Directory_Entry> cont = Read_Root(disk_number);

    
    std::string file_name = path.filename().string();
    std::string parent_name = path.parent_path().filename().string();

    Directory_Entry dir_item;
    parent_dir.first_logical_cluster = 0;
    parent_dir.attributes = static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory);
    std::memset(parent_dir.name, 0, 8);
    std::memset(parent_dir.extension, 0, 3);

    for (auto& part : path_part) {
        int dir_num = -1;
        dir_num = Item_Exist(part, cont, dir_item, dir_num);

        if (dir_num != -1) {
            if (Get_Name(dir_item).compare(file_name) == 0) {
                item = dir_item;
                break;
            }

            if (!(dir_item.attributes == static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory))) {
                return kiv_os::NOS_Error::File_Not_Found;
            }

        }
        else {
            return kiv_os::NOS_Error::File_Not_Found;
        }

        cont.clear();

        parent_dir = dir_item;
        Read_Directory(dir_item, disk_number, fat_table, cont);
    }

    return kiv_os::NOS_Error::Success;
    
}


