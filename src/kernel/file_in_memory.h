#pragma once
#include "../api/api.h"
#include "files.h"
#include <vector>
#include <mutex>

class File_In_Memory : public File {

    kiv_os::THandle handle;
    std::recursive_mutex mutex;
    char* vector;
    size_t size;
    size_t position = 0;
    
    void* area;
    const size_t area_size;
    

public:
    explicit File_In_Memory(size_t size);

    explicit File_In_Memory(void* src_buffer, size_t size);

    template <typename T>
    explicit File_In_Memory(const std::vector<T>& vector)
        : File_In_Memory(sizeof(T)* vector.size())
    {
        memcpy(this->area, vector.data(), this->area_size);
    }

    kiv_os::NOS_Error Write(char* buffer, size_t size, size_t& written);
    kiv_os::NOS_Error Read(char* buffer, size_t size, size_t& read);


    // Closes the stream and frees the associated memory area.
    virtual ~File_In_Memory();

};
