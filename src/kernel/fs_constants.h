#pragma once
#include <cstdint>

const size_t SECTOR_SIZE = 512;

const size_t SECTORS_IN_ROOT = 14;
const size_t SECTOR_ENTRIES = 16;
const size_t ROOT_ENTRIES = SECTORS_IN_ROOT * SECTOR_ENTRIES; //224
const uint16_t FIRST_DATA_SEC = 31;
const uint16_t TOTAL_SECTORS = 2880;
const uint16_t ADDRESS_START = 2;
const uint16_t ADDRESS_END = 2848; 
const uint16_t SECTORS_IN_FAT = 9;

//entries size in bytes

const uint16_t DIR_FILENAME_SIZE = 8;
const uint16_t DIR_EXTENSION_SIZE = 3;
const uint16_t DIR_FIRST_CLUST_SIZE = 2;
const uint16_t DIR_FILESIZE = 4;


//FAT values

const uint16_t UNUSED = 0x00;
const uint16_t RESERVED_START= 0xFF0;
const uint16_t RESERVED_END= 0xFF6;
const uint16_t BAD_CLUSTER= 0xFF7;
const uint16_t LAST_CLUSTER_START= 0xFF8;
const uint16_t LAST_CLUSTER_END = 0xFFF; 


/*
****** FAT12 ORGANIZATION ******   
*	1 cluster = 1 sector  (Sector_Per_Cluster = 1)
*	sector size - 512 bytes
*	first cluster addres - 2  (0 and 1 are reserved)
*	FAT_Count = 2
*	Total_Sector = 2880
*	Sector_per_FAT = 9
* 
*			
*			 BOOT	  FAT1		  FAT2		    ROOT			    DATA AREA
*			 ___________________________________________________________________________
*			|1sec| 9sectors  | 9sectors   | 14sectors	  | 2847 sectors				|
*			|0	0|1			9|10		18|19			32|33						2879|					
*			|512b| 4608b	 | 4608b	  | 7168b		  |								|
*			|____|___________|____________|_______________|_____________________________|
* 
* 
* 
****boot sector**** 
* sector 0

****FAT table 1****
* sectors 1 - 9
*
* 
****FAT table 2****
* sectors 10 - 18
* 
* 
****root directory****
* sectors 19 - 32
* 
*  when 
*  224 entries, 1 entry = 32b
*
* 
* DIR ENTRY
* 
* 
* 
* 
****data area****
* sectors 33 - 2879
*
*	16 directory entries 
*	1 dir entrie	- 32 bytes
* 
*		filename			- 8 bytes
*		extension			- 3 bytes
*		atributes			- 1 bytes
*		reserved			- 2 bytes
*		creation time		- 2 bytes
*		creation date		- 2 bytes
*		last access date	- 2 bytes
*		ignore				- 2 bytes
*		last write time		- 2 bytes
*		last write date		- 2 bytes
*		first cluster		- 2 bytes		
*		file size			- 4 bytes		with flag dir and volume size is 0
* 
* 
****** FAT ******
* 
* index 0 - n
* index 0 and 1 reserved !!
* 
* example FAT  file.txt start cluster 2 -> cluster 3 -> cluster 5 end file
* 0x000 - empty cluster
* 
* index		|	values 
* 0			|	reserved
* 1			|	reserved
* 2			|	3
* 3			|	5
* 4			|	0
* 5			|	0xFFF
* 6			|	0
* 7			|	0
* 8			|	0
* 
* 
* 
* open/read
* When DOS is asked to retrieve a file, DOS first looks at the directory entry for the file.  The �first cluster number� field points to the first cluster that the file is stored in.  This data is then retrieved.  DOS then looks at the entry in the FAT table for the corresponding cluster. 
* 
* 
* 
*/