#pragma once

#include "../api/hal.h"
#include "../api/api.h"
#include "fs_constants.h"

#include <cstdint>
#include <string>
#include <filesystem>
#include <sstream>


namespace fs = std::filesystem;
struct Boot_Sector {
	
	uint16_t bytes_per_sector;
	uint8_t sectors_per_cluster;
	uint16_t reserved_sectors;
	uint8_t num_of_fats;
	uint16_t max_root_entries;

	uint16_t total_sectors;
	uint8_t ignore;

	uint16_t sectors_per_fat;
	uint16_t sectors_per_track;
	uint16_t number_of_heads;

	char reserved[10];
	uint8_t boot_signature;
	uint32_t volume_id;
	char volume_label[11];
};

struct Boot_Record {
	uint16_t bytes_per_sector;
	uint8_t sectors_per_cluster;
	uint16_t reserved_sectors;
	uint8_t num_of_fats;
	uint16_t max_root_entries;

	uint16_t total_sectors;
	uint8_t ignore;

	uint16_t sectors_per_fat;
	uint16_t sectors_per_track;
	uint16_t heads_count;

	char reserved[10];
	uint8_t boot_signature;
	uint32_t volume_id;
	char volume_label[11];
};
struct Directory_Entry {						//size 32 byte
	char name[8];								//
	char extension[3];							// without dot
	uint8_t attributes;							// kiv_os::NFile_Attributes attributes represented by 2 byte
	char reserved[10];							//
	uint16_t last_write_time;					//TODO			
	uint16_t last_write_date;					//TODO
	uint16_t first_logical_cluster;				//  
	uint32_t file_size;							// dir size = 0

};

// disk 
bool Read_From_Disk(uint8_t disk_num, kiv_hal::TDisk_Address_Packet &disk_sector);
bool Write_To_Disk(uint8_t disk_num, kiv_hal::TDisk_Address_Packet disk_sector);
bool Read_Disk(uint64_t sectors, uint64_t first_cluster, char* buffer, uint8_t disk_number);
bool Write_Disk(uint16_t sectors, uint64_t first_cluster, char* buffer, uint8_t disk_number);

//Table
std::vector<uint16_t> Convert_To_Long(std::vector<uint8_t> unconverted_fat);
std::vector<uint8_t> Convert_To_Short(std::vector<uint16_t> converted_fat);
bool Fat_Tabs_Consystency(std::vector<uint8_t> first_tab, std::vector<uint8_t> second_tab);
kiv_os::NOS_Error Load(uint8_t disk_number, kiv_hal::TDrive_Parameters& params, Boot_Sector& boot_sector, std::vector <uint16_t>& fat_table);
bool Update_Tabs(uint8_t disk_number, std::vector<uint16_t>& fat_table);

uint16_t Clusters_Count(std::vector<uint16_t>& fat_table, uint16_t first_cluster);
inline void Remove_Directory_Entry(Directory_Entry& entry);
bool is_Dir_Empty(const std::vector<Directory_Entry> items);
bool is_Root(fs::path& path);
uint16_t Free_Cluster_Index(std::vector<uint16_t> fat_table);
std::vector<std::string> Relativ_Path_Content(fs::path& path);
std::string Get_Name(Directory_Entry& item);
int Item_Exist(std::string folder, std::vector<Directory_Entry> folder_cont, Directory_Entry& dir_item, int dir_num);
kiv_os::NOS_Error Create_Folder(uint8_t disk_number, uint8_t attributes, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table);
kiv_os::NOS_Error Create_File(uint8_t disk_number, uint8_t attributes, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table);
std::vector<Directory_Entry> Read_Root(uint8_t disk_number);
kiv_os::NOS_Error Read_Directory(Directory_Entry& current_dir, uint8_t disk_number, std::vector<uint16_t>& fat_table, std::vector<Directory_Entry>& entries);
kiv_os::NOS_Error Find(fs::path& path, uint8_t disk_number, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table);
std::vector<size_t> Clusters_List(std::vector<uint16_t>& fat_table, uint16_t first_cluster);
kiv_os::NOS_Error Delete_Folder_Content(uint8_t disk_number, std::vector<Directory_Entry>& dir_cont, Directory_Entry& item);
uint64_t Allocate_New_Cluster(uint8_t disk_number, uint16_t start_cluster, std::vector<uint16_t> fat_table);
kiv_os::NOS_Error Delete_Folder(uint8_t disk_number, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table);