#pragma once

#include "../api/api.h"
#include "process.h"
#include "semaphore.h"
#include <string>
#include <mutex>
#include <map>
#include <vector>
#include <Windows.h>
#include <filesystem>
#include <iostream>
#include <set>


extern HMODULE User_Programs;

enum class Process_Status {
    Ready = 0,
    Running = 1,
    Zombie = 2
};

class Wait_For_Listener {
public:
    std::unique_ptr<Semaphore> semaphore;

    kiv_os::THandle source;
    kiv_os::THandle target;

    Wait_For_Listener(std::unique_ptr<Semaphore> semaphore, kiv_os::THandle target)
        : semaphore(std::move(semaphore)), target(target) {};
};

struct Thread {
    std::thread thread;
    kiv_os::THandle handle;
    kiv_os::NOS_Error exit_code = kiv_os::NOS_Error::Success;
};


class Process {
public:
    Process(kiv_os::THandle handle, kiv_os::THandle std_in, kiv_os::THandle std_out, std::string program_name,
        std::string working_directory)
        : handle(handle), std_in(std_in), std_out(std_out), program_name(program_name),
        working_directory(working_directory) {};

    std::recursive_mutex mutex; 

    kiv_os::THandle handle; 

    std::string program_name;

    kiv_os::THandle std_in;
    kiv_os::THandle std_out;

    kiv_os::THandle parent_process; 

    std::map<kiv_os::NSignal_Id, kiv_os::TThread_Proc> signal_handlers; 

    std::set<kiv_os::THandle> owned_handles;
    
    kiv_os::NOS_Error exit_code = kiv_os::NOS_Error::Success;

    Process_Status status = Process_Status::Ready; 

    std::string working_directory;

    std::map<kiv_os::THandle, std::shared_ptr<Thread>> threads; 
};

class Process_Control_Block {
public:
    std::shared_ptr<Process> Add_Process(kiv_os::THandle handle, kiv_os::THandle std_in, kiv_os::THandle std_out, std::string program_name);
    void Remove_Process(kiv_os::THandle handle);

    std::shared_ptr<Process> Find_Process(kiv_os::THandle handle);

    std::vector<std::shared_ptr<Process>> Get_Processes();
    std::recursive_mutex pcb_mutex;
   
private:
    std::map<kiv_os::THandle, std::shared_ptr<Process>> table;
};

class Thread_Control_Block {
public:
    std::recursive_mutex thread_mutex;
    std::map<kiv_os::THandle, kiv_os::THandle> thread_to_parent_process; //firt thread, second process
    std::map<kiv_os::THandle, std::vector<std::shared_ptr<Wait_For_Listener>>> listeners;
};



std::string Process_working_Dir();
void Set_Process_Working_Dir(std::filesystem::path working_dir);
//void Get_Current_Process(std::shared_ptr<Process>& proces);
kiv_os::THandle Get_Current_Process_Handle();
void Add_Owned_Handle(kiv_os::THandle process_handle, kiv_os::THandle file_handle);
Process_Control_Block* Get_PCB();

size_t __stdcall Default_Signal_Handler(const kiv_hal::TRegisters& regs);
//void Thread_Execute(bool is_process);
//void Thread_Entrypoint(kiv_os::TThread_Proc threadproc, kiv_hal::TRegisters& regs, bool is_process, Semaphore* s);
kiv_hal::TRegisters Prepare_Process_Context(unsigned short std_in, unsigned short std_out, char* args);
//void run_in_a_thread(kiv_os::TThread_Proc t_threadproc, kiv_hal::TRegisters& registers, bool is_process, Semaphore* s);
void Create_Process(kiv_hal::TRegisters& regs);
void Create_Thread(kiv_hal::TRegisters& regs);
void Handle_Clone(kiv_hal::TRegisters& regs);
void Handle_Wait_For(kiv_hal::TRegisters& regs);
void Handle_Read_Exit_Code(kiv_hal::TRegisters& regs);
void Handle_Exit(kiv_hal::TRegisters& regs);
void Signal(kiv_os::NSignal_Id signal_id, const std::shared_ptr<Process> process);
void Signal_All_Processes(kiv_os::NSignal_Id signal_id);
void Shutdown_All_Processes();
void Handle_Shutdown(kiv_hal::TRegisters& regs);
void Handle_Register_Signal_Handler(kiv_hal::TRegisters& regs);
void Handle_Process(kiv_hal::TRegisters& regs);