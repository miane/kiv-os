#include "proc_fs.h"
#include <algorithm>

kiv_os::NOS_Error Proc_FS::Remove(fs::path path) {
	return kiv_os::NOS_Error::Permission_Denied;
}


struct Proc_Entry {
	char file_name[15];
	kiv_os::THandle handle;
	char work_dir[25];
};


kiv_os::NOS_Error Proc_FS::Open(fs::path path, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, std::shared_ptr<File>& file) {

	if (!(static_cast<uint8_t>(attributes) & static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only))) {
		// ProcFS is read-only
		return kiv_os::NOS_Error::Permission_Denied;
	}
	
	auto PCB = Get_PCB();
	auto& mutex = PCB->pcb_mutex;
	std::lock_guard<std::recursive_mutex> guard(mutex);
	auto processes = PCB->Get_Processes();

	std::vector<Proc_Entry> entries;

	std::string name = path.filename().string();
	if (strcmp(name.c_str(), "procfs") == 0) 
	{
		for (auto process : processes) {
			
			Proc_Entry e;
			std::string proc_info = "";

			proc_info.append(process.get()->program_name);
			std::memcpy(e.file_name, proc_info.c_str(), 15);
			proc_info = "";
			e.handle = process.get()->handle;
			proc_info.append(process.get()->working_directory);
			std::memcpy(e.work_dir, proc_info.c_str(), 25);

			entries.push_back(e);
		}

		file = std::make_shared<File_In_Memory>(entries);
		return kiv_os::NOS_Error::Success;
	}


	return kiv_os::NOS_Error::File_Not_Found;
}


kiv_os::NOS_Error Proc_FS::Set_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) {
	return kiv_os::NOS_Error::Permission_Denied;
}


kiv_os::NOS_Error Proc_FS::Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) {
	attributes = kiv_os::NFile_Attributes::Read_Only;

	auto p = path.relative_path();

	std::vector<fs::path> paths;
	std::for_each(p.begin(), p.end(), [&paths](auto& path) {
		if (!path.empty()) {
			paths.push_back(path);
		}
		});

	

	return kiv_os::NOS_Error::File_Not_Found;
}

kiv_os::NOS_Error Proc_FS::Close(fs::path path) {
	return kiv_os::NOS_Error::IO_Error;
}






