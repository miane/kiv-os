#pragma once

#include "..\api\api.h"

void Handle_IO(kiv_hal::TRegisters& regs);
void Initialize_IO();

void Initialize_IO();

kiv_os::THandle Get_Console_In();

kiv_os::THandle Get_Console_Out();



kiv_os::THandle Open_Shell(const char* input_file_name, kiv_os::NOpen_File flags, uint8_t attributes, kiv_os::NOS_Error& error);
void Open_File(kiv_hal::TRegisters& regs);
void Write_File(kiv_hal::TRegisters& regs);
void Read_File(kiv_hal::TRegisters& regs);
void Close_IO(kiv_hal::TRegisters& regs);