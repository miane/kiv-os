#include <iostream>
#include "files.h"
#include <random>


kiv_os::THandle last_handle = 2;


kiv_os::NOS_Error File_Handles::Write(char* buffer, size_t size, size_t& written) {
    std::lock_guard<std::recursive_mutex> guard(rec_mutex);
    return kiv_os::NOS_Error::Success;
}
kiv_os::NOS_Error File_Handles::Read(char* buffer, size_t size, size_t& read) {
   
    return kiv_os::NOS_Error::Success;
}

kiv_os::THandle File_Handles::Add_File(std::shared_ptr<File> file) {
    std::lock_guard<std::recursive_mutex> guard(rec_mutex);
   
    last_handle += rand() % 5 + 1;
    table.insert({ last_handle, file });
    return last_handle;
}

std::shared_ptr<File> File_Handles::Get_File(kiv_os::THandle handle) {
    std::lock_guard<std::recursive_mutex> guard(rec_mutex);
    auto file = table.find(handle);

    return (file != table.end()) ? file->second : nullptr;
}

bool File_Handles::Delete_File(kiv_os::THandle handle) {
   std::lock_guard<std::recursive_mutex> guard(rec_mutex);
    auto original_count = table.count(handle);
    table.erase(handle);

    return original_count - 1 == table.count(handle) && table.count(handle) == 0; // ???
}


