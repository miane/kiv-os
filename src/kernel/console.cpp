#include "console.h"


kiv_os::NOS_Error Console::Write(char* buffer, size_t size, size_t& written) {
	std::lock_guard<decltype(mutex)> lck(mutex);

	kiv_hal::TRegisters regs;
	regs.rax.h = static_cast<decltype(regs.rax.h)>(kiv_hal::NVGA_BIOS::Write_String);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(buffer);
	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(size);

	kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, regs);

	if (regs.rax.r == 0) {
		written = 0;
		return kiv_os::NOS_Error::IO_Error;
	}
	else {
		written = size;
		return kiv_os::NOS_Error::Success;
	}

}

kiv_os::NOS_Error Console::Read(char* buffer, size_t size, size_t& read) {

	return kiv_os::NOS_Error::IO_Error; // console output stream is not readable
}
