#include "fat12.h"
#include <mutex>



Fat_FS::Fat_FS(uint8_t disk_number, kiv_hal::TDrive_Parameters disk_parameters) : disk_number(disk_number), disk_parameters(disk_parameters) {
   
}

bool Fat_FS::Init() {
    std::lock_guard<std::mutex> lock(fat_fs_mutex);
    //fat1 = Load_FAT(SECTORS_IN_FAT, 1, disk_number);        // first tab, 9 sectors / 1 - 9
   // std::vector <uint8_t> fat2 = Load_FAT(SECTORS_IN_FAT, 10, disk_number);       // second tab, 9 sectors / 10 - 18
   // if (!Fat_Tabs_Consystency(fat1, fat2)) return false;
    return true;
}

//mkdir / md todo
kiv_os::NOS_Error Fat_FS::Create(uint8_t disk_number, uint8_t attributes, Directory_Entry& item, Directory_Entry& parent_dir, std::vector<uint16_t>& fat_table) {
    
    
    uint16_t next_free_cluster = Free_Cluster_Index(fat_table);
    if (next_free_cluster == 0) {
        return kiv_os::NOS_Error::Not_Enough_Disk_Space;
    }
    item.first_logical_cluster = next_free_cluster;
    item.file_size = 0;

    kiv_os::NOS_Error status = Create_Folder(disk_number, attributes, item, parent_dir, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }
   
    // save fat table where free cluster is marked occupied
    fat_table.at(next_free_cluster) = 4095; // mark of folder end destination
    if (!Update_Tabs(disk_number, fat_table)) 
    {
        return kiv_os::NOS_Error::Directory_Not_Empty;
    }
     

    std::vector<char> buffer;
    buffer.resize(SECTOR_SIZE, 0);

    if (item.attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {

        Directory_Entry self = { 0 };
        std::memset(self.name, 0x20, 8);
        self.name[0] = '.';
        std::memset(self.extension, 0x20, 3);
        self.attributes = item.attributes;
        self.last_write_time = item.last_write_time;
        self.last_write_date = item.last_write_date;
        self.first_logical_cluster = item.first_logical_cluster;
        self.file_size = item.file_size;

        Directory_Entry parent = { 0 };
        std::memset(parent.name, 0x20, 8);
        parent.name[0] = '.';
        parent.name[1] = '.';
        std::memset(parent.extension, 0x20, 3);
        parent.attributes = parent_dir.attributes;
        parent.last_write_time = parent_dir.last_write_time;
        parent.last_write_date = parent_dir.last_write_date;
        parent.first_logical_cluster = parent_dir.first_logical_cluster;
        parent.file_size = parent_dir.file_size;

        std::memcpy(buffer.data(), &self, sizeof(Directory_Entry));
        std::memcpy(buffer.data() + sizeof(Directory_Entry), &parent, sizeof(Directory_Entry));
    }

    uint64_t start_sector = FIRST_DATA_SEC + item.first_logical_cluster;
    uint16_t sector_count = 1;
    if (!Write_Disk(sector_count, start_sector, buffer.data(), disk_number)) {
        return kiv_os::NOS_Error::IO_Error;
    }

    return kiv_os::NOS_Error::Success;
}

//
kiv_os::NOS_Error Fat_FS::Remove(fs::path path) {
    std::lock_guard<std::mutex> lock(fat_fs_mutex);

    Directory_Entry item;
    Directory_Entry parent_dir;
   
    std::vector<uint16_t> fat_table;
    Boot_Sector boot_sector;
    kiv_os::NOS_Error status = Load(disk_number, disk_parameters, boot_sector, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    status = Find(path, disk_number, item, parent_dir, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }
    std::vector<Directory_Entry> dir_cont;
    if (item.attributes == static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
        
        Read_Directory(item, disk_number, fat_table, dir_cont);
        if (!is_Dir_Empty(dir_cont)) {
            return kiv_os::NOS_Error::Directory_Not_Empty;
        }
    }
    status = Delete_Folder_Content(disk_number, dir_cont, item);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    fat_table.at(item.first_logical_cluster) = 0; // mark of folder end destination
    if (!Update_Tabs(disk_number, fat_table))
    {
        return kiv_os::NOS_Error::Directory_Not_Empty;
    }

    status = Delete_Folder(disk_number, item, parent_dir, fat_table);

    return status;

}

//
kiv_os::NOS_Error Fat_FS::Read_Dir(uint8_t disk_number, uint8_t attributes, Directory_Entry& item, std::vector<uint16_t> fat_table, std::vector<kiv_os::TDir_Entry>& entries) {
    
    std::vector <Directory_Entry> dir_cont;
    auto error = Read_Directory(item, disk_number, fat_table, dir_cont);
       
    kiv_os::TDir_Entry e;
    entries.clear();

    for (auto& entry : dir_cont) {
        if (entry.name[0] != '\0') {
            std::string name = Get_Name(entry);
            std::fill(e.file_name, e.file_name + 12, '\0');
            std::memcpy(e.file_name, name.c_str(), 12);
            e.file_attributes = entry.attributes;
            entries.push_back(e);
        }
    }

    return kiv_os::NOS_Error::Success;

}


kiv_os::NOS_Error Fat_FS::Open(fs::path path, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, std::shared_ptr<File>& file) {

    Directory_Entry item;
    Directory_Entry parent_dir;
    std::vector<uint16_t> fat_table;
    Boot_Sector boot_sector;
   
    kiv_os::NOS_Error status = Load(disk_number, disk_parameters, boot_sector, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    status = Find(path, disk_number, item, parent_dir, fat_table);
    if (status == kiv_os::NOS_Error::File_Not_Found) {
        if (flags == kiv_os::NOpen_File::fmOpen_Always) { //if must exist
            return kiv_os::NOS_Error::File_Not_Found;
        }
            if (parent_dir.attributes != static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
                return kiv_os::NOS_Error::Permission_Denied;
            }
            
            path = path.has_filename() ? path : path.parent_path();
            std::vector<std::string> path_part = Relativ_Path_Content(path);
            
            std::string parent_name = path.parent_path().filename().string();;
            std::string file_name;
            std::string file_extention = "";
           /*
            if (attributes != kiv_os::NFile_Attributes::Directory) {
                if (Get_Name(parent_dir).compare(parent_name) == 0) 
                {
                    file_name = path.stem().string();
                    file_extention = path.extension().string();
                    std::memcpy(item.name, file_name.c_str(), 8);
                    std::memcpy(item.extension, file_extention.c_str(), 3);
                    item.attributes = static_cast<uint8_t>(attributes);
                    item.file_size = 0;
                    status = Create_File(disk_number, item.attributes, item, parent_dir, fat_table);
                }
                else
                {
                    return kiv_os::NOS_Error::Permission_Denied;
                }
            }
            */


            if (parent_dir.first_logical_cluster == 0) 
            {
                if(path.has_extension()){
                    file_name = path.stem().string();
                    file_extention = path.extension().string().substr(1, 3);
                    
                }
                else { 
                    file_name = path_part.at(0); 
                }
                std::memcpy(item.name, file_name.c_str(), 8);
                std::memcpy(item.extension, file_extention.c_str(), 3);
                item.attributes = static_cast<uint8_t>(attributes);
                status = Create(disk_number, item.attributes, item, parent_dir, fat_table);
            }
            while (true) {
                status = Find(path, disk_number, item, parent_dir, fat_table);
                if (status == kiv_os::NOS_Error::File_Not_Found) {

                    parent_name = path.parent_path().filename().string();
                    file_name = path.stem().string();
                    file_extention = path.extension().string();

                    //if dont exist only last 
                    if (Get_Name(parent_dir).compare(parent_name) == 0 && parent_dir.attributes == static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
                        std::memcpy(item.name, file_name.c_str(), 8);
                        std::memcpy(item.extension, file_extention.c_str(), 3);
                        item.attributes = static_cast<uint8_t>(attributes);
                        status = Create(disk_number, item.attributes, item, parent_dir, fat_table);
                    }
                    else
                    {
                        size_t i = 0;
                        size_t parent = 0;
                        for (auto part : path_part) {
                            if (Get_Name(parent_dir).compare(part) == 0)
                            {
                                parent = i;
                            }
                            i++;
                        }
                        file_name = path_part.at(parent + 1);
                        std::memcpy(item.name, file_name.c_str(), 8);
                        std::memcpy(item.extension, file_extention.c_str(), 3);
                        item.attributes = static_cast<uint8_t>(attributes);
                        status = Create(disk_number, item.attributes, item, parent_dir, fat_table);
                    }
                }
                else 
                {
                    break;
                }
            }
        
    }
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    if ((item.attributes == static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) && !(attributes == kiv_os::NFile_Attributes::Directory)) {
        return kiv_os::NOS_Error::Permission_Denied;
    }

    if ((item.attributes == static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only)) && !(attributes == kiv_os::NFile_Attributes::Read_Only)) {
        return kiv_os::NOS_Error::Permission_Denied;
    }
    else if (attributes == kiv_os::NFile_Attributes::Read_Only) {
        item.attributes |= static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only);
    }

    if (item.attributes == static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
        std::vector <kiv_os::TDir_Entry> entries;
        status = Read_Dir(disk_number, item.attributes, item, fat_table, entries);
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }
        
        file = std::make_shared<File_In_Memory>(entries);
        
    }
    else {
        file = std::make_shared<Fat_File>(disk_number, disk_parameters, path, fat_table);
    }
    

    return kiv_os::NOS_Error::Success;
}


kiv_os::NOS_Error Fat_FS::Set_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) {
    std::lock_guard<std::mutex> lock(fat_fs_mutex);

    Directory_Entry item;
    Directory_Entry parent_dir;
    std::vector<uint16_t> fat_table;
    Boot_Sector boot_sector;

    kiv_os::NOS_Error status = Load(disk_number, disk_parameters, boot_sector, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    status = Find(path, disk_number, item, parent_dir, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }
    item.attributes = static_cast<uint8_t>(attributes);

    std::vector<Directory_Entry> root_cont = Read_Root(disk_number);
    if (parent_dir.first_logical_cluster == 0) { // is root dir

        Directory_Entry dir_item;
        int dir_num = -1;
        dir_num = Item_Exist(Get_Name(item), root_cont, dir_item, dir_num);
        if (dir_num == -1)
        {
            return kiv_os::NOS_Error::File_Not_Found;
        }
        std::memcpy(root_cont.data(), &item, sizeof(Directory_Entry));
        if (!Write_Disk(SECTORS_IN_ROOT, 19, reinterpret_cast<char*>(fat_table.data()), disk_number)) {
            return kiv_os::NOS_Error::IO_Error;
        }
        else {
            return kiv_os::NOS_Error::Success;
        }
    }
    
    //TODO
    
    Update_Tabs(disk_number, fat_table);

    return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Fat_FS::Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) {
    std::lock_guard<std::mutex> lock(fat_fs_mutex);

    Directory_Entry item;
    Directory_Entry parent_dir;
    std::vector<uint16_t> fat_table;
    Boot_Sector boot_sector;

    kiv_os::NOS_Error status = Load(disk_number, disk_parameters, boot_sector, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    status = Find(path, disk_number, item, parent_dir, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }
    
    attributes = static_cast<kiv_os::NFile_Attributes>(item.attributes);
    return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Fat_FS::Close(fs::path path) {
    return kiv_os::NOS_Error::IO_Error;
}

kiv_os::NOS_Error Fat_File::Write(char* buffer, size_t size, size_t& written) {
    std::lock_guard<decltype(mutex)> lock(mutex);


    Directory_Entry item;
    Directory_Entry parent_dir;
    Boot_Sector boot_sector;
   

    kiv_os::NOS_Error status = Load(disk_number, disk_parameters, boot_sector, converted_fat);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    status = Find(path, disk_number, item, parent_dir, converted_fat);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }
    written = 0;
    if (item.attributes & static_cast<uint16_t>(kiv_os::NFile_Attributes::Directory)) {
        return kiv_os::NOS_Error::Invalid_Argument;
    }
    if (size == 0) {
        return kiv_os::NOS_Error::Success;
    }
   
    size_t cluster_count = Clusters_Count(converted_fat, item.first_logical_cluster);
    const size_t allocated = cluster_count * SECTOR_SIZE;
    size_t fill_bytes = 0;
    size_t new_bytes = 0;
    size_t to_write = 0;

    if ((position + size) > allocated) {
        if (position > allocated) {
            fill_bytes = static_cast<size_t>(position) - allocated;
            new_bytes = fill_bytes + size;
        }
        else {
            new_bytes = size - (allocated - static_cast<size_t>(position));
        }
    }


    size_t new_clusters = (new_bytes + SECTOR_SIZE - 1) / SECTOR_SIZE;
    for (int i = 0; i < new_clusters; i++) {
        auto a = Allocate_New_Cluster(disk_number, item.first_logical_cluster, converted_fat);
    }
    
    
    size_t written_bytes = 0;


    std::vector<char> cluster_buffer;
    cluster_buffer.resize(SECTOR_SIZE, 0);
    std::vector<size_t> sector_list = Clusters_List(converted_fat, item.first_logical_cluster);
   
    size_t cluster_offset = position % SECTOR_SIZE;
    to_write = SECTOR_SIZE - cluster_offset;
    if (to_write > size) {
        to_write = size;
    }
    /*
    std::vector<char> sector_buffer;
    size_t buffer_size = cluster_count * SECTOR_SIZE;
    sector_buffer.resize(buffer_size, 0);
    
    Read_Disk(cluster_count, cluster, sector_buffer.data(), disk_number);

    std::memcpy(cluster_buffer.data() + cluster_offset, buffer, to_write);
    */
    
    for(int i = 0; i < sector_list.size(); i++){

        Read_Disk(cluster_count, FIRST_DATA_SEC + (sector_list.at(i)), cluster_buffer.data(), disk_number);
        std::memcpy(cluster_buffer.data() + cluster_offset, buffer, to_write);

        if (!Write_Disk(1,FIRST_DATA_SEC + (sector_list.at(i)), cluster_buffer.data(), disk_number)) {
            return kiv_os::NOS_Error::IO_Error;
        }
        written_bytes += to_write;
    }

    if ((cluster_offset + size) > item.file_size) {
        item.file_size = static_cast<uint32_t>(cluster_offset + size);
    }

    written += written_bytes;
    
    Update_Tabs(disk_number, converted_fat);

    position += written;

    std::vector<Directory_Entry> cont;
    int pos = -1;

    if (parent_dir.first_logical_cluster == 0) { // is root dir
        cont = Read_Root(disk_number);
        //Directory_Entry dir_item;
        for (int i = 0; i < ROOT_ENTRIES; i++) {
            if (std::strcmp(cont[i].name, item.name) == 0 && pos == -1) {
                pos = i;
            }
        }
        if (pos != -1) { // is root dir
            std::memcpy(cont.data() + pos, &item, sizeof(Directory_Entry));
            if (!Write_Disk(SECTORS_IN_ROOT, 19, reinterpret_cast<char*>(cont.data()), disk_number)) {
                return kiv_os::NOS_Error::IO_Error;
            }
            return kiv_os::NOS_Error::Success;
        }
    }

    return kiv_os::NOS_Error::Success;


}

kiv_os::NOS_Error Fat_File::Read(char* buffer, size_t size, size_t& read) {
    std::lock_guard<decltype(mutex)> lock(mutex);

    Directory_Entry item;
    Directory_Entry parent_dir;
    std::vector<uint16_t> fat_table;
    Boot_Sector boot_sector;
   

    kiv_os::NOS_Error status = Load(disk_number, disk_parameters, boot_sector, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    status = Find(path, disk_number, item, parent_dir, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    read = 0;
    if (size == 0) {
        return kiv_os::NOS_Error::Success;
    }

    size_t cluster_count = Clusters_Count(fat_table, item.first_logical_cluster);
    size_t to_read;
    bool buffer_full = false;
    size_t offset = position % SECTOR_SIZE;
    std::vector<size_t> sector_list = Clusters_List(fat_table, item.first_logical_cluster);
    
    for (int i = 0; i < sector_list.size(); i++) {
        to_read = cluster_count * SECTOR_SIZE - offset;

        if (read + to_read > size) {
            to_read = size - read;
            buffer_full = true;
        }
        if (!(item.attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory))) {
            if (read + to_read > item.file_size - position) {
                to_read = item.file_size - position - read;
                buffer_full = true;
            }
        }
        size_t cluster = sector_list[i] + static_cast <size_t>(31);
        std::vector<char> sector_buffer;

        size_t buffer_size = cluster_count * SECTOR_SIZE;
        sector_buffer.resize(buffer_size, 0);
        Read_Disk(cluster_count, cluster, sector_buffer.data(), disk_number);

        if (buffer_size < size) {
            buffer_size = size;
        }
        else if (size > 0 && (buffer_size - offset) > size) {
            buffer_size = size + offset;
        }

        std::memcpy(buffer, sector_buffer.data() + offset, buffer_size - offset);
        read += to_read;

        if (buffer_full) {
            break;
        }
    }

    position += read;
    return kiv_os::NOS_Error::Success;
}
   