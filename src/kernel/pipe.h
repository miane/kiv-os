#pragma once

#include <vector>
#include <map>
#include <memory>
#include "semaphore.h"
#include "../api/api.h"
#include "files.h"

class Pipe {

    size_t write_ptr;
    size_t read_ptr;
    bool close_in = false;
    

    std::vector<char> pipe_buffer;
    //char* buffer;
    size_t capacity;

    Semaphore empty_semaphore;
    Semaphore full_semaphore;

    

    //const size_t PIPE_BUFFER_SIZE = 1024;

public:
    explicit Pipe(size_t capacity);

    void Close() {
        if (!this->close_in) {
            this->empty_semaphore.wait();

                pipe_buffer.push_back(static_cast<char>(kiv_hal::NControl_Codes::EOT));
                this->close_in = true;

                this->full_semaphore.notify();
        }
        else {
            pipe_buffer.clear();
            this->close_in = false;

            
        }
    }

    std::vector<char> Read(size_t size, size_t& read);
    size_t Write(char* buffer, size_t size);
};

class PipeFile : public File {
    std::recursive_mutex mutex;
public:
    PipeFile(std::shared_ptr<Pipe> pipe);
    //PipeFile(std::shared_ptr<Pipe> pipe): pipe(pipe) {};
    virtual kiv_os::NOS_Error Write(char* buffer, size_t size, size_t& written) override;
    virtual kiv_os::NOS_Error Read(char* buffer, size_t size, size_t& read) override;
    void Close() override;
protected:
    std::shared_ptr<Pipe> pipe;
};
/*
class PipeFileIn : public PipeFile {
public:
    PipeFileIn(std::shared_ptr<Pipe> pipe) : PipeFile(pipe) {};
    virtual kiv_os::NOS_Error Read(char* buffer, size_t size, size_t& read) override;
    virtual kiv_os::NOS_Error Write(char* buffer, size_t size, size_t& written) override;
};

class PipeFileOut : public PipeFile {
public:
    PipeFileOut(std::shared_ptr<Pipe> pipe) : PipeFile(pipe) {};
    virtual kiv_os::NOS_Error Read(char* buffer, size_t size, size_t& read) override;
    virtual kiv_os::NOS_Error Write(char* buffer, size_t size, size_t& written) override;
};
*/
void Create_Pipe(kiv_hal::TRegisters& regs);