#pragma once

#include "vfs.h"
#include "process.h"
#include "files.h"
#include "file_in_memory.h"

class Proc_FS : public VFS {
public:

    //Removes directory / rmdir
    kiv_os::NOS_Error Remove(fs::path path) override;

    kiv_os::NOS_Error Open(fs::path path, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, std::shared_ptr<File>& file) override;

    kiv_os::NOS_Error Set_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) override;

    kiv_os::NOS_Error Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) override;

    kiv_os::NOS_Error Close(fs::path path) override;
};
