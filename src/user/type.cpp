#include "type.h"
#include <string>

bool Is_EOF(char ch) {
	// 0, Ctrl + C, Ctrl + D, Ctrl + Z
	return ch == 0 || ch == 3 || ch == 4 || ch == 26;
}

extern "C" size_t __stdcall type(const kiv_hal::TRegisters & regs) {

	// references std_in and std_out
	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	std::string out_string = "";
	size_t written;

	//arguments
	const char* command = reinterpret_cast<const char*>(regs.rdi.r);

	if (strlen(command) == 0) {
		out_string = "The syntax of the command is incorrect.\n";
		kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
		//kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
		return 1;
	}
	else {
		uint8_t attributes = static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only);
		kiv_os::THandle handle = kiv_os_rtl::Open_File(command, kiv_os::NOpen_File::fmOpen_Always, attributes);
		if (handle == kiv_os::Invalid_Handle) {
			out_string = "File not found.\n";
			kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
			//kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
			return 1;
		}

		size_t read;
		const size_t buffer_size = 4096;
		char buffer[buffer_size + 1];
		buffer[buffer_size] = '\0';
		char end[2] = { 0, 0 };

		while (kiv_os_rtl::Read_File(handle, buffer, buffer_size, read) ) {
			if (read == 0) break;

			if (!kiv_os_rtl::Write_File(std_out, buffer, read, written)) {
				break;
			}
			
		}
		const char* new_line = "\n";
		kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);
		kiv_os_rtl::Close_Handle(handle);
	}

	return 0;
}