#include "command_parser.h"


std::vector<Command> Tokenizer(char* input) {

	std::vector<Command> commands = std::vector<Command>();
	std::istringstream stream(input);
	std::string token;
	std::string file_name;

	bool end = false;
	while(stream >> token) {
		if (token == "")
		{
			end = true;
		}
		else {

			Command command;
			command.program_name = token;
			command.parameters = "";

			while (stream >> token) {
				if (token == "")
				{
					end = true;
					commands.push_back(command);
					break;
				}
				else if (token == "|") //pipe
				{
					command.pipe = true;
					break;
				}
				else if (token == "<") //in
				{
					command.in = true;
					command.file_name = "";
					if (stream >> token) {
						command.file_name.append(token);
					}
				}

				else if (token == ">") //out
				{
					command.out = true;
					end = true;
					command.file_name = "";
					if (stream >> token) {
						command.file_name.append(token);
					}
					commands.push_back(command);
					break;
				}
				else {
					command.parameters.append(token);
				}
			}
			if (end) { break; }
			commands.push_back(command);
			//end = true;
		}
					
		if (end) { break; }
	}

	token.clear();
	stream.clear();
	file_name.clear();
	
	return commands;
}


