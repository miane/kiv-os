#include "freq.h"
#include <string>
#include <vector>

inline bool Stop_Call(char ch) {
	return ch == 0 || ch == 3 || ch == 4 || ch == 26;
}


extern "C" size_t __stdcall freq(const kiv_hal::TRegisters & regs) {

	// references std_in and std_out
	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	std::string out_string = "";
	const char new_line = '\n';
	size_t written;
	size_t read;

	const char* command = reinterpret_cast<const char*>(regs.rdi.r);
	//if arguments are specified
	if (strlen(command) != 0) {
		out_string = "The syntax of the command is incorrect.\n";
		kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
		return 0;
	}

	//const int buffer_size = 1;
	//char buffer[buffer_size];
	//size_t f_buffer_size = 1024;
	const int f_buffer_size = 256;
	std::vector<int> freqs(f_buffer_size);

	std::vector<char> buffer;
	size_t buffer_size = 1024;
	buffer.resize(buffer_size);

	//signalized flag (eot,eof,...)
	bool stop_called = false; 
	//reading cycle
	while (!stop_called)
	{
		
		if (kiv_os_rtl::Read_File(std_in, (char*)buffer.data(), buffer_size, read)) {

			for (size_t i = 0; i < read; i++) {
				//check if didn't come stop call
				stop_called = Stop_Call(buffer[i]);
				

				//add freq to vector
				unsigned int index = (unsigned char)buffer[i];
				freqs[index]++;
			}
		}
		else {
			stop_called = true;
		}
		buffer.clear();
		buffer.resize(buffer_size);
	} 
	//print new line
	kiv_os_rtl::Write_File(std_out, &new_line, 1, written);

	//print frequency table
	int size;
	const size_t out_buffer_size = 30;
	char out_buff[out_buffer_size];
	unsigned char i = 0;

	size_t c;
	for (auto& item : freqs) {
		if (item > 0) {
			size = sprintf_s(out_buff, "0x%hhx : %d\n", i, item);			
			kiv_os_rtl::Write_File(std_out, out_buff, size, c);
		}
		i++;
	}

	return 0;

}