#include "dir.h"
#include <string.h>
#include "rtl.h"
#include <vector>
#include <string>


// print all subdirectories -> /S
bool recursive = false;

// print all -> /A
bool specific_file = false;


//find " " when command have more path
std::vector<std::string> Partition(std::string &cmd) {
	std::string separator = " ";
	std::vector<std::string> dirs;
	size_t start_path = 0;
	size_t pointer = 0;

	while (start_path != cmd.size()) {
		while ((pointer = cmd.find(separator, pointer)) != std::string::npos) {

			std::string path = cmd.substr(start_path, pointer);		//path
			dirs.push_back(path);

			pointer++;
			start_path = pointer;
		}

		std::string path = cmd.substr(start_path, pointer);		//path
		dirs.push_back(path);
		start_path = cmd.size();
	}
	return dirs;
}

void Remove_Spaces(std::string &cmd) {
	std::string old = " ";
	size_t start = 0;
	while ((start = cmd.find(old, start)) != std::string::npos) {
		cmd = cmd.erase(start, 1);
	}
}

void Replace_Spaces(std::string& cmd) {
	std::string old = " ";
	std::string new_name = " .";
	size_t start = 0;
	while ((start = cmd.find(old, start)) != std::string::npos) {
		cmd.replace(cmd.begin(), cmd.end(), new_name);
	}
}

bool Print_Dir_Entry(kiv_os::TDir_Entry entry, kiv_os::THandle out_file) {
	for (int i = 0; i < 12; i++) {
		if (entry.file_name[i] == '�' || entry.file_name[i] == '\0') {
			entry.file_name[i] = ' ';
		}
	}

	std::string str(entry.file_name);
	str.resize(12);

	if (entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
		str[8] = ' ';
		str.append("    <DIR>\n");
	}
	else if (entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Volume_ID)) {
		str[8] = ' ';
		str.append("    <VOLUME_ID>\n");
	}
	else {
		str.append("\n");
	}

	size_t written;
	bool result = kiv_os_rtl::Write_File(static_cast<kiv_os::THandle>(out_file), str.c_str(), str.size(), written);
	if (!result || written == 0) {
		return false;
	}

	return true;
}

bool Process_Path(std::string path, kiv_os::THandle std_out) {

	kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
	auto attributes = static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory);
	kiv_os::THandle handle = kiv_os_rtl::Open_File(path.c_str(), flags, attributes);
	size_t read = 0;

	std::vector <std::string> stack;
	std::vector <kiv_os::TDir_Entry> buffer;

	buffer.resize(30);

	while (kiv_os_rtl::Read_File(handle, (char*)buffer.data(), buffer.size() * sizeof(kiv_os::TDir_Entry), read)) {
		
		if (read == 0) 
			break;
	
		for (auto& entry : buffer) {

			if (entry.file_name[0] == '\0') {
				break;
			}

			if (!specific_file && entry.file_attributes == static_cast<uint8_t>(kiv_os::NFile_Attributes::Volume_ID)) {
				continue;
			}
				
			if (!Print_Dir_Entry(entry, std_out)) {
				break;
			}

			if (recursive && (entry.file_attributes == static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory))) {
				if (std::strncmp(entry.file_name, ".", 1) != 0 && std::strncmp(entry.file_name, "..", 2) != 0) {
					stack.push_back(path + "\\" + entry.file_name);
				}
			}
		}
	}
	
	kiv_os_rtl::Close_Handle(handle);

	for (auto& dir : stack) {
		size_t written;
		const char* new_line = "\n";

		kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);
		kiv_os_rtl::Write_File(std_out, dir.c_str(), dir.size(), written);
		kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);
		if (!Process_Path(dir, std_out)) {
			return false;
		}
	}

	return true;
}

//find " and erase
void Remove_q(std::string &cmd) {
	std::string old = "\"";
	size_t start = 0;
	while ((start = cmd.find(old, start)) != std::string::npos) {
		cmd = cmd.erase(start, 1);
	}
}

//find param. /S -> recursive = true and erase that param. from command
void Find_Param(std::string &cmd) {
	size_t found_atr_s = cmd.find("/S");
	size_t found_atr_ss = cmd.find("/s");
	if (found_atr_s != std::string::npos) {
		cmd = cmd.erase(found_atr_s, 3);
		recursive = true;
	}
	if (found_atr_ss != std::string::npos) {
		cmd = cmd.erase(found_atr_ss, 3);
		recursive = true;
	}
	size_t found_atr_a = cmd.find("/A"); // || "/a");
	size_t found_atr_aa = cmd.find("/a");
	if (found_atr_a != std::string::npos) {
		cmd = cmd.erase(found_atr_a, 3);
		specific_file = true;
	}
	if (found_atr_aa != std::string::npos) {
		cmd = cmd.erase(found_atr_aa, 3);
		specific_file = true;
	}
}

extern "C" size_t __stdcall dir(const kiv_hal::TRegisters & regs) {

	// reference std_out
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	//attributes
	char* command = reinterpret_cast<decltype(command)>(regs.rdi.r);

	std::vector<std::string> paths;

	recursive = false;
	specific_file = false;

	// check arguments 
	if (regs.rdi.r != 0 && strlen(command) > 0) {

		//string is better
		std::string cmd(command);
		//find param. /S -> recursice = true and erase that param. from command
		Find_Param(cmd);
		//find " and erase
		Remove_q(cmd);

		//path -> vector and find " " when command have more path
		paths = Partition(cmd);

		if (paths.empty()) {
			paths.push_back("");
		}
	}
	else {
		paths.push_back("");
	}

	for (auto path : paths) {
		Process_Path(path, std_out);
	}

	kiv_os_rtl::Close_Handle(std_out);

	return 0;
}