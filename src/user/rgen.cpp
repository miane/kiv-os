#include "rgen.h"
#include <string>
#include <vector>
#include <mutex>
#include <random>


struct Rgen_Parameters {
	
	std::mutex mutex;

	kiv_os::THandle std_in;
	bool stop_called = false;

	inline kiv_os::THandle Get_Std_In() {
		std::lock_guard lck(mutex);
		return std_in;
	}

	inline void Set_Std_In(kiv_os::THandle handle) {
		std::lock_guard lck(mutex);
		std_in = handle;
	}

	inline bool Get_Call() {
		std::lock_guard lck(mutex);
		return stop_called;
	}

	inline void Set_Call(char ch) {
		std::lock_guard lck(mutex);
		if( ch == 0 || ch == 3 || ch == 4 || ch == 26){
				stop_called = true;
		}
	}
};


size_t __stdcall Stop_Call_Checker(const kiv_hal::TRegisters& regs) {
	Rgen_Parameters& parameters = *reinterpret_cast<Rgen_Parameters*>(regs.rdi.r);
	const kiv_os::THandle std_in = parameters.Get_Std_In();

	// read buffer
	const size_t buffer_size = 256;
	char buffer[buffer_size];
	size_t read;

	do {
		bool result = kiv_os_rtl::Read_File(std_in, buffer, buffer_size, read);
		if (result) {
			parameters.Set_Call(buffer[0]);
			break;
		}
		else {
			parameters.stop_called = true;
		}

	} while (!parameters.Get_Call());

	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);
	return 0;
}


extern "C" size_t __stdcall rgen(const kiv_hal::TRegisters & regs) {

    // references std_in and std_out
	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	const char* command = reinterpret_cast<const char*>(regs.rdi.r);

	std::string out_string = "";
	size_t written;
	
	//if arguments are specified
	if (strlen(command) != 0) {
		out_string = "The syntax of the command is incorrect.\n";
		kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
		kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
		return 1;
	}


	Rgen_Parameters parameters;
	parameters.Set_Std_In(std_in);
	kiv_os::THandle handle;
	
	
	if (!kiv_os_rtl::Create_Thread(&Stop_Call_Checker, reinterpret_cast<char*>(&parameters), handle)) {
		kiv_os_rtl::Exit(kiv_os::NOS_Error::Out_Of_Memory);		
		return 1;
	}
	
	//generate
	std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution;

	while (!parameters.Get_Call()) {
		float ran_number = distribution(generator);
		out_string = std::to_string(ran_number);
		out_string.append("\n");

		kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
	}
/*
	std::vector<kiv_os::THandle> single_handle;
	single_handle.push_back(handle);
	uint8_t index;

	kiv_os_rtl::Wait_For(single_handle.data(), 1, index);
	*/

	return 0;
}