#pragma once

#include "..\api\api.h"
#include <atomic>

namespace kiv_os_rtl {

	extern std::atomic<kiv_os::NOS_Error> Last_Error;


	// NOS_File_System
	bool Read_File(const kiv_os::THandle file_handle, char* const buffer, const size_t buffer_size, size_t& read);
	//zapise do souboru identifikovaneho deskriptor data z buffer o velikosti buffer_size a vrati pocet zapsanych dat ve written
	//vraci true, kdyz vse OK
	//vraci true, kdyz vse OK

	bool Write_File(const kiv_os::THandle file_handle, const char* buffer, const size_t buffer_size, size_t& written);
	//zapise do souboru identifikovaneho deskriptor data z buffer o velikosti buffer_size a vrati pocet zapsanych dat ve written
	//vraci true, kdyz vse OK
	//vraci true, kdyz vse OK
	
	/*
	TODO 
	bool Seek(const kiv_os::THandle file_handle, );
	*/

	bool Close_Handle(kiv_os::THandle file_handle); 	
	bool Delete_File(const char* file_name); 
	bool Set_Working_Dir(const char* path); 
	bool Get_Working_Dir(char* buffer, size_t buffer_size, size_t& written); 
	bool Set_File_Attributes(const char* file_name, uint8_t attributes); 
	bool Get_File_Attributes(const char* file_name, uint8_t& attributes); 
	bool Create_Pipe(kiv_os::THandle* pipe_handles);
	kiv_os::THandle Open_File(const char* file_name, kiv_os::NOpen_File flags, uint8_t attributes);



	//NOS_Process
	bool Create_Thread(kiv_os::TThread_Proc entrypoint, const char* data, kiv_os::THandle& thread_handle);
	bool Clone_Process(const char* program, const char* args, kiv_os::THandle std_in, kiv_os::THandle std_out, kiv_os::THandle& process_handle);
	bool Read_Exit_Code(const kiv_os::THandle process_handle, kiv_os::NOS_Error& exit_code);
	bool Wait_For(const kiv_os::THandle* process_handles, const size_t process_handles_size, size_t& signaling_handle_index);
	bool Exit(kiv_os::NOS_Error exit_code);
	bool Shutdown();
}