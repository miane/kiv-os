#include "find.h"
#include <string>
#include <vector>

inline bool Stop_Call(char ch) {
	return ch == 0 || ch == 3 || ch == 4 || ch == 26;
}

//find " and get file and string
void Get_Args(std::string &cmd, std::string& str, std::vector <std::string>& files) {
	std::string qot = "\"";
	size_t start = 0;
	size_t end = 0;
	int i = 0;
	char last_char = '\0';
	while (cmd.size() != 0)
	{
		auto len = cmd.size();
		if (cmd[start] == '\"') {
			while ((end = cmd.find(qot, end + 1)) != std::string::npos) {
				if (str.empty()) {
					size_t temp = end - start - 1;
					str = cmd.substr(start + 1, temp);
					cmd = cmd.erase(start, (end + 1));
				}
				else {
					size_t temp = end - start - 1;
					files.push_back(cmd.substr(start + 1, temp));
					cmd = cmd.erase(start, (end + 1));
				}

			}
		}
		else if (cmd[start] == ' ') {
			cmd = cmd.erase(start, 1);
		}
		else {
			if (cmd[i] == ' ' || i == len) {
				files.push_back(cmd.substr(start, i));
				cmd = cmd.erase(start, i);
				i = 0;
			}
			i++;
		}
	}	
}

//find param. /V /U and erase that param from command
bool Find_Param_c(std::string& cmd) {
	size_t found_c;
	size_t found_cb;
	if (found_c = cmd.find("/c") != std::string::npos) {
		cmd = cmd.erase(found_c - 1, 2);
		return true;
	}
	else if (found_cb = cmd.find("/C") != std::string::npos) {
		cmd = cmd.erase(found_cb - 1, 2);
		return true;
	}
	return false;
}

//find param. /V /U and erase that param from command
bool Find_Param_v(std::string& cmd) {
	size_t found_inv;
	size_t found_invb;
	if (found_inv = cmd.find("/v") != std::string::npos) {
		cmd = cmd.erase(found_inv - 1, 2);
		return true;
	}
	if (found_invb = cmd.find("/V") != std::string::npos) {
		cmd = cmd.erase(found_invb - 1, 2);
		return true;
	}
	return false;
}

bool Process_Path(std::string* path, std::string str, kiv_os::THandle std_out, bool inverse, bool count, const kiv_os::THandle handle) {

	size_t written;
	size_t read;
	std::string new_line = "\n";


	if (path != nullptr) {
		kiv_os_rtl::Write_File(std_out, new_line.data(), new_line.size(), written);
		std::string title = "---------- " + *path;
		if (!kiv_os_rtl::Write_File(std_out, title.data(), title.size(), written)) {
			return false;
		}
	}

	//reading cycle
	const size_t buffer_size = 4096;
	char buffer[buffer_size + 1];
	buffer[buffer_size] = '\0';
	std::string temp = "";
	std::string out = "";
	int c_out = 0;

	bool stop_called = false;

	while (!stop_called) {
		
		bool result = kiv_os_rtl::Read_File(handle, buffer, buffer_size, read);

		for (int i = 0; i < read; i++) {
			stop_called = Stop_Call(buffer[i]);
			if (stop_called) break;

			if (buffer[i] == '\n' || buffer[i] == '\r' || ( stop_called && temp.size() > 0)) {
				if (temp.find(str) != std::string::npos && !inverse) {
					out.append(temp);
					out.append(new_line);
					c_out++;
				}
				else if (temp.find(str) == std::string::npos && inverse) {

					out.append(temp);
					out.append(new_line);
					c_out++;
				}
				temp = "";
			}
			else
			{
				//temp.append(buffer[i]);
				temp.push_back(buffer[i]);
			}
		}

		if (buffer_size > read) {
			stop_called = true;
		}

	}

	kiv_os_rtl::Write_File(std_out, new_line.data(), new_line.size(), written);

	if (count) {
		std::string count_str = ": " + std::to_string(c_out) + "\n";
		if (!kiv_os_rtl::Write_File(std_out, count_str.data(), count_str.size(), written)) {
			return false;
		}
	}
	else {
		if (!kiv_os_rtl::Write_File(std_out, out.data(), out.size(), written)) {
			return false;
		}
	}	
	return true;
}

inline bool Check_quot(std::string &cmd) {
	int quote_count = 0;
	char qot = '\"';
	for (size_t i = 0; i < cmd.length(); i++)
	{
		if (cmd[i] == '\"')
		{
			quote_count++;
		}
	}
	if (quote_count < 2) return false;
	if (quote_count % 2 == 1) return false;
	return true;
}

extern "C" size_t __stdcall find(const kiv_hal::TRegisters & regs) {


	// references std_in and std_out
	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	std::string out_string = "";
	size_t written;

	//load arguments
	const char* command = reinterpret_cast<const char*>(regs.rdi.r);

	if (strlen(command) == 0) {
		out_string = "FIND: Parameter format not correct.\n";
		kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
		return 1;
	}

	//arguments to string
	std::string cmd(command);

	if (!Check_quot(cmd)) {
		out_string = "FIND: Parameter format not correct.\n";
		kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
		return 1;
	}

	//find param. /C /V   and erase that param. from command
	bool count = Find_Param_c(cmd);
	bool inverse = Find_Param_v(cmd);

	std::string str;
	std::vector<std::string> files;
	//find " and erase
	Get_Args(cmd, str, files);

	if (files.empty()) {
		
		if (!Process_Path(nullptr, str, std_out, inverse, count, std_in)) {
			return 1;
		}
	}
	else {
		for (auto& file : files) {
			kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
			auto attributes = static_cast<uint8_t>(static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only));
			kiv_os::THandle handle = kiv_os_rtl::Open_File(file.c_str(), flags, attributes);
			
			if (handle == kiv_os::Invalid_Handle) {
				return 1;
			}
			if(!Process_Path(&file, str, std_out, inverse, count, handle)) {
				return 1;
			}
			std::string new_line = "\n";
			kiv_os_rtl::Write_File(std_out, new_line.data(), new_line.size(), written);

			kiv_os_rtl::Close_Handle(handle);
		}
	}

	return 0;
}