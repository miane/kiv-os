#include "rtl.h"

std::atomic<kiv_os::NOS_Error> kiv_os_rtl::Last_Error;

kiv_hal::TRegisters Prepare_SysCall_Context(kiv_os::NOS_Service_Major major, uint8_t minor) {

    kiv_hal::TRegisters regs;
    regs.rax.h = static_cast<uint8_t>(major);
    regs.rax.l = minor;
    regs.flags.carry = 0;
    return regs;
}

kiv_hal::TRegisters Prepare_SysCall_Context(kiv_os::NOS_Service_Major major, uint8_t minor, uint8_t nclone) {

    auto regs = Prepare_SysCall_Context(major, minor);
    regs.rcx.r = nclone;
    return regs;
}


bool
kiv_os_rtl::Read_File(const kiv_os::THandle file_handle, char *const buffer, const size_t buffer_size, size_t &read) {

    auto regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Read_File));
    regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
    regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
    regs.rcx.r = buffer_size;

    auto result = kiv_os::Sys_Call(regs);
    read = regs.rax.r;
    return result;
}

bool kiv_os_rtl::Write_File(const kiv_os::THandle file_handle, const char *buffer, const size_t buffer_size, size_t &written) {

    auto regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Write_File));
    regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
    regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
    regs.rcx.r = buffer_size;

    auto result = kiv_os::Sys_Call(regs);
    written = regs.rax.r;
    return result;
}

bool kiv_os_rtl::Shutdown() {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Shutdown));

    return kiv_os::Sys_Call(regs);
}

bool kiv_os_rtl::Create_Thread(kiv_os::TThread_Proc entrypoint, const char* data, kiv_os::THandle &thread_handle) {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));
    regs.rcx.r = static_cast<decltype(regs.rcx.r)>(kiv_os::NClone::Create_Thread); 
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(entrypoint);
    regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(data);
    //regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(&data);


    auto result = kiv_os::Sys_Call(regs);
    thread_handle = regs.rax.x;
    return result;
}

bool kiv_os_rtl::Clone_Process(const char *program, const char *args, kiv_os::THandle std_in, kiv_os::THandle std_out, kiv_os::THandle &process_handle) {

    auto regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone), static_cast<uint8_t>(kiv_os::NClone::Create_Process));
    regs.rbx.e = (std_in << 16) | std_out;
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(program);
    regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(args);

    auto result = kiv_os::Sys_Call(regs);
    process_handle = regs.rax.x;
    return result;
}

bool kiv_os_rtl::Wait_For(const kiv_os::THandle* process_handles, const size_t process_handles_size, size_t &signaling_handle_index) {

    auto regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Wait_For));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(process_handles);
    regs.rcx.l = process_handles_size;

    auto result = kiv_os::Sys_Call(regs);
    signaling_handle_index = regs.rax.l;

    return result;
}

bool kiv_os_rtl::Read_Exit_Code(const kiv_os::THandle process_handle, kiv_os::NOS_Error &exit_code) {

    auto regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Read_Exit_Code));
    regs.rdx.x = process_handle;

    auto result = kiv_os::Sys_Call(regs);
    exit_code = static_cast<kiv_os::NOS_Error>(regs.rcx.x);
    return result;
}


//TODO dod�lat co chyb�


bool kiv_os_rtl::Exit(kiv_os::NOS_Error exit_code) {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Exit));
    regs.rcx.x = static_cast<decltype(regs.rcx.x)>(exit_code);

    return kiv_os::Sys_Call(regs);
}

bool kiv_os_rtl::Close_Handle(kiv_os::THandle file_handle) {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Close_Handle));
    regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
   
    const bool result = kiv_os::Sys_Call(regs);
    return result;

}

bool kiv_os_rtl::Delete_File(const char* file_name) {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Delete_File));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(file_name);

    const bool result = kiv_os::Sys_Call(regs);
    return result;
}

bool kiv_os_rtl::Set_Working_Dir(const char* path) {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Set_Working_Dir));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(path);

    const bool result = kiv_os::Sys_Call(regs);
    return result;
}

bool kiv_os_rtl::Get_Working_Dir(char* buffer, size_t buffer_size, size_t& written) {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Get_Working_Dir));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(buffer);
    regs.rcx.r = buffer_size;
    
    const bool result = kiv_os::Sys_Call(regs);
    written = regs.rax.r;
    return result;
}

bool kiv_os_rtl::Set_File_Attributes(const char* file_name, uint8_t attributes) {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Set_File_Attribute));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(file_name);
    regs.rdi.i = attributes;

    const bool result = kiv_os::Sys_Call(regs);
    return result;
}

bool kiv_os_rtl::Get_File_Attributes(const char* file_name, uint8_t& attributes) {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Get_File_Attribute));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(file_name);

    const bool result = kiv_os::Sys_Call(regs);
    attributes = regs.rdi.i;
    return result;

}

bool kiv_os_rtl::Create_Pipe(kiv_os::THandle* pipe_handles) {

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Create_Pipe));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(pipe_handles);

    const bool result = kiv_os::Sys_Call(regs);
    return result;
}

kiv_os::THandle kiv_os_rtl::Open_File(const char* file_name, kiv_os::NOpen_File flags, uint8_t attributes)
{
    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Open_File));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(file_name);
    regs.rcx.l = static_cast<decltype(regs.rcx.l)>(flags);
    regs.rdi.i = static_cast<uint8_t>(attributes);


    if (kiv_os::Sys_Call(regs)) {
        return regs.rax.x;
    }
    return kiv_os::Invalid_Handle;
}
