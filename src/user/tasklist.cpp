#include "tasklist.h"
#include <vector>
#include <string>


struct Proc_Entry {
	char file_name[15];
	kiv_os::THandle handle;
	char work_dir[25];
};



extern "C" size_t __stdcall tasklist(const kiv_hal::TRegisters & regs) {

	// reference std_out
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	char* command = reinterpret_cast<decltype(command)>(regs.rdi.r);
	size_t written;
	size_t read;
	kiv_os::NOpen_File flags = static_cast<kiv_os::NOpen_File>(0);
	uint8_t attributes = static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only);
	std::vector<Proc_Entry> entries;


	// read procfs directory items
	kiv_os::THandle handle = kiv_os_rtl::Open_File("procfs", flags, attributes);

	std::vector <Proc_Entry> buffer;
	buffer.resize(5);

	while (kiv_os_rtl::Read_File(handle, (char*)buffer.data(), buffer.size() * sizeof(Proc_Entry), read)) {
		if (read == 0) break;

		for (auto& entry : buffer) {
		
			entries.push_back(entry);
		}
	}
	

	
	

	//when 
	if (!entries.empty()) {
	
		//printing header
		const char* new_line = "\n";
		kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);
		char buffer[50] = { 0 };
		snprintf(buffer, sizeof(buffer), "%-15s   %6s   %25s\n", "PROGRAM", "PID", "PROCESS DIRECTORY");
		kiv_os_rtl::Write_File(std_out, buffer, strnlen(buffer, sizeof(buffer)), written);
		kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);
		snprintf(buffer, sizeof(buffer), "%-15s   %6s  %25s\n", "===============", "======", "==============================");
		kiv_os_rtl::Write_File(std_out, buffer, strnlen(buffer, sizeof(buffer)), written);
		kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);

		//read files and dir in profcs... maybe wrong atributes directory ... (and correct maybe read_only)
		for (auto entry : entries) {
			if (entry.file_name[0] == '\0') break;
			snprintf(buffer, sizeof(buffer), "%-15s   %6i  %25s\n", entry.file_name, entry.handle, entry.work_dir);
			kiv_os_rtl::Write_File(std_out, buffer, strnlen(buffer, sizeof(buffer)), written);
			kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);
		}

	}
	else {
		const char* message = "Failed to open PCB table or PCB table is empty\n";
		kiv_os_rtl::Write_File(std_out, message, strlen(message), written);
	}

	return 0;
}