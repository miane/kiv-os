#pragma once
#include <vector>
#include <string>
#include <iostream>     
#include <sstream>      
#include "../api/api.h"

class Command
{
public:
	std::string program_name;
	std::string parameters;
	std::string file_name;
	bool pipe = false;
	bool in = false;
	bool out = false;
	kiv_os::THandle redir_in = kiv_os::Invalid_Handle;
	kiv_os::THandle redir_out = kiv_os::Invalid_Handle;
};

std::vector<Command> Tokenizer(char* input);




