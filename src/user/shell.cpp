#include "shell.h"
#include "rtl.h"
#include "command_executor.h"
#include "command_parser.h"

#include <iostream>
#include <thread>
#include <chrono>

extern bool is_echo_on;
const size_t SIZE_TEXT = 5000;

kiv_os::THandle pipe_out;


size_t __stdcall shell(const kiv_hal::TRegisters &regs) {


	// references std_in and std_out
	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	// read buffer
	const size_t buffer_size = 256;
	std::vector<char> buffer(buffer_size);
	size_t written; 
	size_t read;

	const char* intro = "Welcome to the KIV/OS shell \n";
						
	kiv_os_rtl::Write_File(std_out, intro, strlen(intro), written);

	size_t counter;
	char prompt[SIZE_TEXT];

	

	const char* beak = ">";
	const char* new_line = "\n";

	do {
		
		if(is_echo_on) {
			kiv_os_rtl::Get_Working_Dir(prompt, SIZE_TEXT, counter);
			kiv_os_rtl::Write_File(std_out, prompt, strlen(prompt), written);
			kiv_os_rtl::Write_File(std_out, beak, strlen(beak), written);

		}

		if (kiv_os_rtl::Read_File(std_in, buffer.data(), buffer_size, read)) {


			if ((read > 0) && (read == buffer_size)) read--;	
			buffer[read] = 0;	//null-terminated string
			
			kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);

			if (read == 1) continue;
			
			//command parser
		
			std::vector<Command> commands = Tokenizer(buffer.data());
			
			if (strcmp(commands[0].program_name.data(), "exit") == 0) break;
			
			bool existed = true;

			for (auto command : commands) {
				if (!Program_is_exist(command.program_name.data())) {
					std::string error = "Unknown command.\n";
					//kiv_os_rtl::Exit(kiv_os::NOS_Error::Unknown_Error);
					kiv_os_rtl::Write_File(std_out, error.data(), error.size(), written);
					existed = false;
					break;
				}
				if (!commands[0].pipe) {
					if (strcmp(command.program_name.data(), "cd") == 0) {
						Execute_CD(command, std_out);
						continue;
					}
					//else if (strcmp(command.program_name.data(), "exit") == 0) break;
					else
					{
						Execute_Program(command, std_in, std_out);
					}
				}
			}

			if (commands.at(0).pipe && existed) {
				Execute_Pipe_Programs(commands, std_in, std_out);

			}
			kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);

			//Execute(commands, std_in, std_out);


		}
		else
			break;	//EOF
	} while (strcmp(buffer.data(), "exit") != 0);
	
	const char* bye_message = "\nShell exiting...\n";
	kiv_os_rtl::Write_File(std_out, bye_message, strlen(bye_message), written);

    return 0;
}
