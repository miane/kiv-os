#include "echo.h"
#include <string>


size_t __stdcall echo(const kiv_hal::TRegisters& regs) {

	// reference std_out
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	//arguments
	char* command = reinterpret_cast<decltype(command)>(regs.rdi.r);

	const char* new_line = "\n";
	size_t written;

	if (strlen(command) == 0) {
		if (is_echo_on) {
			std::string out_string = "Echo is on.\n";
			kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
		}
		else {
			std::string out_string = "Echo is off.\n";
			kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
		}
	}
	else {
		if (strcmp(command, "on") == 0) {
			is_echo_on = true;
		}
		else if (strcmp(command, "off") == 0) {
			is_echo_on = false;
		}
		else if (strcmp(command, "/?") == 0) {
			const char* echo_help = "Displays messages, or turns command-echoing on or off.\n" \
				"ECHO [ON | OFF].\n" \
				"ECHO [message]\n"\
				"Type ECHO without parameters to display the current echo setting.\n";

			kiv_os_rtl::Write_File(std_out, echo_help, strlen(echo_help), written);
		}
		else {
			
			if (command[0] == '\"' && command[strlen(command) - 1] == '\"') {
				// if command starts and ends with double quotes - remove them
				command++;
				command[strlen(command) - 1] = '\0';
			}
			
			// output text
			kiv_os_rtl::Write_File(std_out, command, strlen(command), written);	
		}		
	}
	// output new line
	//kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);

	return 0;
}