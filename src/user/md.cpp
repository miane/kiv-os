#include "md.h"
#include <string>

extern "C" size_t __stdcall md(const kiv_hal::TRegisters & regs) {

	// reference std_out
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	std::string out_string = "";
	size_t written;

	// new directory name
	const char* command = reinterpret_cast<const char*>(regs.rdi.r);

	// if dir name not specified
	if (strlen(command) == 0) {
		const char* error_message = "Name not specified.\n";
		kiv_os_rtl::Write_File(std_out, error_message, strlen(error_message), written);
		kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
	}
	else {
		
		kiv_os::NOpen_File flags = static_cast<kiv_os::NOpen_File>(0);
		// set attributes type directory
		auto attributes = static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory);

		kiv_os::THandle handle = kiv_os_rtl::Open_File(command, flags, attributes);
	
		if (handle == kiv_os::Invalid_Handle) {
			out_string = "Directory not found.\n";
			kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
			kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
			return 1;
		}

		kiv_os_rtl::Close_Handle(handle);


	}

	return 0;
}