#pragma once

#include "command_parser.h"
#include "../api/api.h"
#include "rtl.h"

bool Program_is_exist(const char* name);
void Execute_CD(Command command, const kiv_os::THandle std_out);
void Execute_Program(Command command, const kiv_os::THandle in, const kiv_os::THandle out);
void Execute_Pipe_Programs(std::vector<Command> commands, const kiv_os::THandle in, const kiv_os::THandle out);

