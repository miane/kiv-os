#include "shutdown.h"
#include <iostream>

extern "C" size_t __stdcall shutdown(const kiv_hal::TRegisters & regs) {
	
	kiv_os_rtl::Shutdown();
	std::cout << "Shutting down the system..." << std::endl;
	return 0;
}