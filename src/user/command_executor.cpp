#include "command_executor.h"
#include <vector>
#include <algorithm>
#include <cctype>
#include <string>
#include <map>
#include <vector>

const size_t SIZE_TEXT = 5000;

bool Program_is_exist(const char* name) {
	std::vector<const char*> available_commands = {"echo", "cd" , "dir", "md", "rd",
											"type", "find", "sort", "tasklist", 
											"shutdown", "exit", "rgen", "freq", "shell"};

	for (const char* program : available_commands) {

		if (strcmp(program, name) == 0) {
			return true;
		}
	}
	return false;
}

void Execute_CD(Command command, const kiv_os::THandle std_out) {

	size_t written;

	if (!command.parameters.empty()) {
		if (!kiv_os_rtl::Set_Working_Dir(command.parameters.c_str())) {
			std::string error = "Directory not found\n";
			kiv_os_rtl::Write_File(std_out, error.data(), error.size(), written);
		}
		//kiv_os_rtl::Get_Working_Dir(prompt, SIZE_TEXT, counter);
	}
	else {
		std::string error = "Directory not specified\n";
		kiv_os_rtl::Write_File(std_out, error.data(), error.size(), written);
	}	
}



void Execute_Program(Command command, const kiv_os::THandle std_in, const kiv_os::THandle std_out) {

	char err[256] = { 0 };
	size_t written;
	kiv_os::THandle handle;
	kiv_os::THandle file_redir_in = kiv_os::Invalid_Handle;
	kiv_os::THandle file_redir_out = kiv_os::Invalid_Handle;


	if (command.in) {
		kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
		auto attributes = static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only);
		file_redir_in = kiv_os_rtl::Open_File(command.file_name.data(), flags, attributes);
		if (file_redir_in == kiv_os::Invalid_Handle) {
			snprintf(err, sizeof(err) - 1, "shell: could not open input file `%s` for writing\n", command.file_name.data());
			kiv_os_rtl::Write_File(std_out, err, strlen(err), written);
			return;
		}
		else {
			kiv_os_rtl::Clone_Process(command.program_name.data(), command.parameters.data(), file_redir_in, std_out, handle);
		}
	}

	else if (command.out) {
		kiv_os::NOpen_File flags = static_cast<kiv_os::NOpen_File>(0);
		file_redir_out = kiv_os_rtl::Open_File(command.file_name.data(), flags, 0);
		if (file_redir_out == kiv_os::Invalid_Handle) {
			snprintf(err, sizeof(err) - 1, "shell: could not open output file `%s` for writing\n", command.file_name.data());
			kiv_os_rtl::Write_File(std_out, err, strlen(err), written);
			return;
		}
		else {
			kiv_os_rtl::Clone_Process(command.program_name.data(), command.parameters.data(), std_in, file_redir_out, handle);
		}
	}
	else {
		kiv_os_rtl::Clone_Process(command.program_name.data(), command.parameters.data(), std_in, std_out, handle);
	}

	//wait for end program
	kiv_os::NOS_Error exitcode;
	bool ec_read = kiv_os_rtl::Read_Exit_Code(handle, exitcode);

	if (!ec_read) {
		snprintf(err, sizeof(err) - 1, "shell: error while reading process exit code\n");
		kiv_os_rtl::Write_File(std_out, err, strlen(err), written);
	}

	if (exitcode != kiv_os::NOS_Error::Success) {
		snprintf(err, sizeof(err) - 1, "shell: command `%s` exited with code %u\n", command.program_name.c_str(), exitcode);
		kiv_os_rtl::Write_File(std_out, err, strlen(err), written);
	}
}


	void Execute_Pipe_Programs(std::vector<Command> commands, const kiv_os::THandle in, const kiv_os::THandle out) {
	
		std::vector<kiv_os::THandle> handles;
		kiv_os::THandle handle;
		char err[256] = { 0 };
		size_t written;
		//kiv_os::THandle file_redir_in = kiv_os::Invalid_Handle;
		//kiv_os::THandle file_redir_out = kiv_os::Invalid_Handle;
		int pos = 0;
	
		

		for (size_t i = 0; i < commands.size() - 1; i++) {
			kiv_os::THandle pipe_handles[2];

			if (kiv_os_rtl::Create_Pipe(pipe_handles)) {

				commands[i].redir_out = pipe_handles[0];
				commands[i + 1].redir_in = pipe_handles[1];
			}

		}
		

		for (int i = static_cast<int>(commands.size()) - 1; i >= 0; i--) {

			if (commands[i].in) {
				kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
				auto attributes = static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only);
				commands[i].redir_in = kiv_os_rtl::Open_File(commands[i].file_name.data(), flags, attributes);
				if (commands[i].redir_in == kiv_os::Invalid_Handle) {
					snprintf(err, sizeof(err) - 1, "shell: could not open input file `%s` for writing\n", commands[i].file_name.data());
					kiv_os_rtl::Write_File(out, err, strlen(err), written);
					return;
				}
			}

			else if (commands[i].out) {
				kiv_os::NOpen_File flags = static_cast<kiv_os::NOpen_File>(0);
				commands[i].redir_out = kiv_os_rtl::Open_File(commands[i].file_name.data(), flags, 0);
				if (commands[i].redir_out == kiv_os::Invalid_Handle) {
					snprintf(err, sizeof(err) - 1, "shell: could not open output file `%s` for writing\n", commands[i].file_name.data());
					kiv_os_rtl::Write_File(out, err, strlen(err), written);
					return;
				}
			}

			kiv_os::THandle redir_in = (commands[i].redir_in == kiv_os::Invalid_Handle) ? in : commands[i].redir_in;
			kiv_os::THandle redir_out = (commands[i].redir_out == kiv_os::Invalid_Handle) ? out : commands[i].redir_out;

			// Create process for new command.
			kiv_os_rtl::Clone_Process(commands[i].program_name.data(), commands[i].parameters.data(), redir_in, redir_out, handle);
			handles.push_back(handle);
			
		}
		

		for (int i = static_cast<int>(commands.size()) - 1; i >= 0; i--) {
			
			kiv_os::THandle handle = handles[i];

			//wait for end program
			kiv_os::NOS_Error exitcode;
			bool ec_read = kiv_os_rtl::Read_Exit_Code(handle, exitcode);

			if (ec_read) {
				//Command& command = commands[i];
				handles.erase(handles.begin() + i);
				
				
				
				char err[256] = { 0 };
				size_t written;
				if (commands[pos].redir_in != kiv_os::Invalid_Handle) {
					bool success = kiv_os_rtl::Close_Handle(commands[pos].redir_in);
					if (!success) {
						snprintf(err, sizeof(err) - 1, "shell: error closing input handle for `%s`\n", commands[pos].program_name.c_str());
						kiv_os_rtl::Write_File(out, err, strlen(err), written);
					}
				}
				if (commands[pos].redir_out != kiv_os::Invalid_Handle) {
					bool success = kiv_os_rtl::Close_Handle(commands[pos].redir_out);
					if (!success) {
						snprintf(err, sizeof(err) - 1, "shell: error closing output handle for `%s`\n", commands[pos].program_name.c_str());
						kiv_os_rtl::Write_File(out, err, strlen(err), written);
					}
				}
				
				pos++;
			}
		}


	}

