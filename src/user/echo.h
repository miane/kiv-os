#pragma once

#include "..\api\api.h"
#include "rtl.h"
#include "shell.h"

extern bool is_echo_on = true;

extern "C" size_t __stdcall echo(const kiv_hal::TRegisters & regs);