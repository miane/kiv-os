#include "rd.h"
#include <string>

extern "C" size_t __stdcall rd(const kiv_hal::TRegisters & regs) {

	// reference std_out
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	std::string out_string = "";
	size_t written;

	const char* command = reinterpret_cast<const char*>(regs.rdi.r);

	if (strlen(command) == 0) {
		out_string = "Name not specified.\n";
		kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
		kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
		return 1;
	}

	if (!kiv_os_rtl::Delete_File(command)) {
		out_string = "Directory not found.\n";
		kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
		kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
		return 1;
	}

	return 0;
}