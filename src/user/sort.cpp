#include "sort.h"
#include <string>
#include <vector>
#include <algorithm>

inline bool Stop_Call(char ch) {
	return ch == 0 || ch == 3 || ch == 4 || ch == 26;
}

bool Get_Path(std::string& cmd, std::string& path) {
	std::string qot = "\"";
	size_t start = 0;
	size_t end = 0;
	int i = 0;
	if (cmd[start] == ' ') {
		cmd = cmd.erase(start, 1);
	}
	
	if (cmd[start] == '\"') {
		while ((end = cmd.find(qot, end + 1)) != std::string::npos) {
			if (path.empty()) {
				size_t temp = end - start - 1;
				path = cmd.substr(start + 1, temp);
				cmd = cmd.erase(start, (end + 1));
				return true;
			}
			else {
				return false;
			}
		}
	}	 
	else {
		path = cmd.substr(start, cmd.length());
		cmd = cmd.erase(start, cmd.length());
		return true;
	}
	return true;
}

std::vector<std::string> Process_Path(std::string* path, const kiv_os::THandle handle, const kiv_os::THandle std_out) {
	std::vector<std::string> lines;
	size_t written;
	size_t read;
	char new_line = '\n';
	//char buffer[1];
	std::vector<char> buffer;
	//const int buffer_size = 1;
	size_t buffer_size = 1024;
	buffer.resize(buffer_size);
	/*
	if (path == nullptr) {
		const int buffer_size = 1;
		buffer.resize(buffer_size);
	}
	else {
		const int buffer_size = 1028;
		buffer.resize(buffer_size);
	}
	*/
	std::string temp = "";

	//signalized flag (eot,eof,...)
	bool stop_called = false;
	//reading cycle
	while (!stop_called)
	{
		if (kiv_os_rtl::Read_File(handle, (char*)buffer.data(), buffer_size, read)) {
			kiv_os_rtl::Write_File(std_out, &new_line, 1, written);

			for (size_t i = 0; i < read; i++) {
				//check if didn't come stop call
				stop_called = Stop_Call(buffer.at(i));
				if (stop_called) {
					break;
				}
				

				if (buffer.at(i) == '\n' || buffer.at(i) == '\r' || (stop_called && temp.size() > 0)) {
					lines.push_back(temp);
					temp = "";
					//kiv_os_rtl::Write_File(std_out, &new_line, 1, written);
				}
				else
				{
					temp.push_back(buffer.at(i));
				}

				
			}
			
		}
		else {
			stop_called = true;
			break;
		}

		if (stop_called) {
			break;
		}
	}
	return lines;
}

inline bool Check_quot(std::string& cmd) {
	int quote_count = 0;
	char qot = '\"';
	for (size_t i = 0; i < cmd.length(); i++)
	{
		if (cmd[i] == '\"')
		{
			quote_count++;
		}
	}
	if (quote_count % 2 == 1) return false;
	return true;
}


extern "C" size_t __stdcall sort(const kiv_hal::TRegisters & regs) {

	// references std_in and std_out
	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	std::string out_string = "";
	const char new_line = '\n';
	size_t written;

	const char* command = reinterpret_cast<const char*>(regs.rdi.r);

	const size_t buffer_size = 512;
	std::vector<char> buffer(buffer_size);
	
	std::vector<std::string> lines;

	if (strlen(command) == 0) {
		lines = Process_Path(nullptr, std_in, std_out);
	}
	else {
		//arguments to string
		std::string cmd(command);
		
		if (!Check_quot(cmd)) {
			out_string = "Input file specified two times.\n";
			kiv_os_rtl::Write_File(std_out, out_string.data(), out_string.size(), written);
			//kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
			return 1;
		}

		//find " and erase
		std::string path;
		Get_Path(cmd, path);

		kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
		auto attributes = static_cast<uint8_t>(static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only));
		kiv_os::THandle handle = kiv_os_rtl::Open_File(path.c_str(), flags, attributes);

		if (handle == kiv_os::Invalid_Handle) {
			return 1;
		}
		lines = Process_Path(&path, handle, std_out);
	}

	kiv_os_rtl::Write_File(std_out, &new_line, 1, written);

	//sort lines
	std::sort(lines.begin(), lines.end());

	//output text
	for each (std::string line in lines) {
		kiv_os_rtl::Write_File(std_out, line.data(), line.size(), written);
		kiv_os_rtl::Write_File(std_out, &new_line, 1, written);
	}

	kiv_os_rtl::Close_Handle(std_in);

	return 0;
}